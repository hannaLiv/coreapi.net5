﻿using CoreMigroup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewModel
{
    public class NhaCungCapPaging 
    {
        public List<NhaCungCap> nhaCungCaps { get; set; }
        public int total { get; set; }
    }
}
