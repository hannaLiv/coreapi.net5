﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewModel
{
    public class UsersControllerViewModel
    {
        public class LoginAsyncViewModel
        {
            public string userName { get; set; }
            public string passWord { get; set; }
        }
        public class SaveNotificationTokensViewModel
        {
            public string token { get; set; }
            public int user_id { get; set; }
        }
    }
}
