﻿using CoreMigroup.Data;
using CoreMigroup.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public interface IUserHelper
    {
        Task<ApplicationUser> GetCurrentUserAsync();
        bool CheckUserLogin();
        List<int> GetListUserIdByType(int type);
    }
    public class UserHelper : IUserHelper
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;
        public UserHelper(IHttpContextAccessor httpContext, UserManager<ApplicationUser> userManager, ApplicationDbContext db)
        {
            _httpContext = httpContext;
            _userManager = userManager;
            _db = db;
        }
        public async Task<ApplicationUser> GetCurrentUserAsync()
        {
            var user = new ApplicationUser();
            try
            {
                user = await _userManager.GetUserAsync(_httpContext.HttpContext.User);
            } catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            
            return user;
        }
        public bool CheckUserLogin()
        {
            var currentUser = _userManager.GetUserAsync(_httpContext.HttpContext.User).Result;
            if(currentUser != null)
            {
                return true;
            }
            return false;

        }

        public List<int> GetListUserIdByType(int type)
        {
            var result = new List<int>();
            //Get user by type
            if(type > 0)
            {

                var x = _db.Users.Where(r => r.LoaiUser == type);
                if (x.Count() > 0)
                    result = x.Select(r => r.Id).ToList();

            }
            return result;
        }
    }
}
