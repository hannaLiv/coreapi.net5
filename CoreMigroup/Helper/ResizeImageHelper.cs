﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public static class ResizeImageHelper
    {
        public static string Image_resize(string input_Image_Path, string output_Image_Path, int width = 150, int height = 150)
        {
            using (Image image = Image.Load(input_Image_Path))
            {
                image.Mutate(x => x
                     .Resize(image.Width > width ? width : image.Width, image.Height > height ? height : image.Height));
                image.Save(output_Image_Path);
                return output_Image_Path;
            }
        }
    }
}
