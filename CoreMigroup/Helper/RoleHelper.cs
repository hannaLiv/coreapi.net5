﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public static class RoleHelper
    {
        public static string ReturnBaseRole(string lsQuyen)
        {
            var quyenAll = "Admin";
            return string.Concat(quyenAll, ",", lsQuyen);
        }
    }
}
