﻿using CoreMigroup.Data;
using CoreMigroup.Enum;
using CoreMigroup.Models;
using CoreMigroup.Repository.FileUploadRepository;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public interface ICrawlingExcelHelper
    {
        Task<int> ImportExcelDoanhNghiepAsync(int idFileUpload);

    }
    public class CrawlingExcelHelper : ICrawlingExcelHelper
    {
        private readonly IWebHostEnvironment _env;
        private readonly IFileUploadRepository _fileUploadReposiotory;
        private readonly ApplicationDbContext _db;
        public CrawlingExcelHelper(IFileUploadRepository fileUploadRepository, ApplicationDbContext db, IWebHostEnvironment env)
        {
            _fileUploadReposiotory = fileUploadRepository;
            _db = db;
            _env = env;
        }
        public async Task<int> ImportExcelDoanhNghiepAsync(int idFileUpload)
        {

            var totalRecord = 0;
            var file = _fileUploadReposiotory.GetById(idFileUpload);
            if(file != null)
            {
                var path = _env.WebRootPath + file.DuongDan;
                FileInfo fileInfo = new FileInfo(path);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(fileInfo))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

                    var start_row = 2;
                    var flag = true;
                    var res = new List<DataDoanhNghiep>();
                    while (flag)
                    {
                        var mst = (worksheet.Cells[start_row, 2].Value ?? "").ToString();
                        var tenCongTy = (worksheet.Cells[start_row, 3].Value ?? "").ToString();
                        var sdt = (worksheet.Cells[start_row, 4].Value ?? "").ToString();
                        var email = (worksheet.Cells[start_row, 5].Value ?? "").ToString();
                        var diaChi = (worksheet.Cells[start_row, 6].Value ?? "").ToString();
                        var fax = (worksheet.Cells[start_row, 7].Value ?? "").ToString();
                        var website = (worksheet.Cells[start_row, 8].Value ?? "").ToString();
                        var tenTiengAnh = (worksheet.Cells[start_row, 9].Value ?? "").ToString();
                        var nguoiDaiDienPL = (worksheet.Cells[start_row, 10].Value ?? "").ToString();
                        var tenVietTat = (worksheet.Cells[start_row, 11].Value ?? "").ToString();
                        var loaiHinhPhapLy = (worksheet.Cells[start_row, 12].Value ?? "").ToString();
                        var tinhTrangHoatDong = (worksheet.Cells[start_row, 13].Value ?? "").ToString().Equals("Đang hoạt động") ? (int)TinhTrangHoatDongDoanhNghiep.DangHoatDong : (int)TinhTrangHoatDongDoanhNghiep.KhongRo;
                        var tinhThanh = (worksheet.Cells[start_row, 14].Value ?? "").ToString();
                        var ngayThanhLap = string.Join("-", (worksheet.Cells[start_row, 15].Value ?? "").ToString().Split("/").Reverse());
                        var maNganhChinh = (worksheet.Cells[start_row, 16].Value ?? "").ToString();
                        var tenNganhChinh = (worksheet.Cells[start_row, 17].Value ?? "").ToString();
                        var nhaMang = (worksheet.Cells[start_row, 18].Value ?? "").ToString();


                        if (string.IsNullOrEmpty(mst))
                        {
                            flag = false;
                            continue;
                        }
                        var d = new DataDoanhNghiep();
                        d.Ma = mst;
                        d.MaSoThue = mst;
                        d.Ten = tenCongTy;
                        d.TenCongTy = tenCongTy;
                        d.SoDienThoai = sdt;
                        d.Email = email;
                        d.DiaChiTruSo = diaChi;
                        d.Fax = fax;
                        d.Website = website;
                        d.TenTiengAnh = tenTiengAnh;
                        d.TenNguoiDaiDien = nguoiDaiDienPL;
                        d.TenVietTat = tenVietTat;
                        d.LoaiHinhPhapLy = loaiHinhPhapLy;
                        d.TinhTrangHoatDong = tinhTrangHoatDong;
                        try
                        {
                            d.NgayThanhLap = Convert.ToDateTime(ngayThanhLap);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            d.NgayThanhLap = DateTime.MinValue;
                            //throw;
                        }

                        d.TinhThanh = tinhThanh;
                        d.MaNganhChinh = maNganhChinh;
                        d.TenNganhChinh = tenNganhChinh;
                        res.Add(d);
                        Console.WriteLine("Đọc dòng thứ" + start_row);
                        start_row++;
                    }
                    var trungDl = _db.DataDoanhNghieps.Select(s => s.MaSoThue).AsEnumerable().Intersect(res.Select(e => e.MaSoThue)).ToList();
                    res = res.Except(res.Where(s => trungDl.Contains(s.MaSoThue))).ToList();
                    try
                    {
                        await _db.DataDoanhNghieps.AddRangeAsync(res);
                        await _db.SaveChangesAsync();
                        totalRecord = res.Count();
                        
                        return totalRecord;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        
                    }
                }
            }
            
            return totalRecord;
        }
    }
}
