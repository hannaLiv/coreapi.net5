﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public class ResponseData<T>
    {
        public bool IsSuccess { get; set; } = false;
        public int TotalRecord { get; set; } = 0;
        public string Message { get; set; } = "";
        public List<T> Datas { get; set; } = new List<T>();
    }
    public class DataTablesResponeData<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
        public string error { get; set; }
    }
}
