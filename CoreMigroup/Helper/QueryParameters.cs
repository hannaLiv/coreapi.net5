﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public class QueryParameters
    {
        public string Keyword { get; set; } = "";
        public int? Index { get; set; } = 1;
        public int? Size { get; set; } = 10;

    }
}
