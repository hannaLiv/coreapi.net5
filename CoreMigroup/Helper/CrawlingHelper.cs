﻿//using OfficeOpenXml;
//using OfficeOpenXml;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Helper
{
    public static class CrawlingHelper
    {
        private static String ColorHexValue(System.Drawing.Color C)
        {
            return C.A.ToString("X2") + C.R.ToString("X2") + C.G.ToString("X2") + C.B.ToString("X2");
        }
        public static List<ThongTin> GenDuLieu()
        {
            var result = new List<ThongTin>();
            var result_thong_tin = new List<ThongTin>();
            //Read file excel here
            string path = "C:\\Users\\Administrator\\Desktop\\tkb.xlsx";
            FileInfo fileInfo = new FileInfo(path);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(fileInfo))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                var lsKhoiLop = new List<KhoiLop>();
                var lsMonHoc = new List<MonHoc>();
                var lsMonHocsKhoi = new List<ThongTin>();


                //get so luong khoi voi quy luat nhu file Iqschool
                var crawling_col = 1;
                var start_row = 2;

                var jump = 4;
                var flag = false;
                var start_id = 1;

                while (!flag && crawling_col < 21)
                {
                    if (!string.IsNullOrEmpty(worksheet.Cells[start_row, crawling_col].Value.ToString()))
                    {
                        var new_khoi_lop = new KhoiLop() { Id = crawling_col, Ten = worksheet.Cells[start_row, crawling_col].Value.ToString() };
                        lsKhoiLop.Add(new_khoi_lop);
                        var flag_mon = false;
                        var crawing_row = 3;

                        while (!flag_mon)
                        {
                            var cd = worksheet.Cells[crawing_row, crawling_col];
                            if (cd.Value != null)
                            {
                                var tenMon = cd.Value.ToString();
                                var maMau = cd.Style.Fill.BackgroundColor.Rgb;
                                var soTiet = (double)worksheet.Cells[crawing_row, crawling_col + 1].Value;
                                var mucDo_col = worksheet.Cells[crawing_row, crawling_col + 3];
                                var m = 0;
                                if (int.TryParse(mucDo_col.Value.ToString(), out m))
                                {

                                    var set_id = 0;
                                    var checker = lsMonHoc.Where(r => r.Ten.Equals(tenMon)).FirstOrDefault();
                                    if (checker == null)
                                    {
                                        set_id = start_id;
                                        start_id++;
                                    }
                                    else
                                    {
                                        set_id = checker.Id;
                                    }
                                    var new_thong_tin = new ThongTin();
                                    var new_mon_hoc = new MonHoc() { Id = set_id, Ten = tenMon, MaMau = maMau, SoTietHoc = soTiet, MucDoUuTien = m };
                                    if (checker == null)
                                    {
                                        lsMonHoc.Add(new_mon_hoc);
                                    }
                                    var new_thong_tin_mon_hoc = new ThongTinMonHoc() { GetMonHoc = new_mon_hoc, SoTiet = soTiet };
                                    new_thong_tin.GetKhoi = new_khoi_lop;
                                    new_thong_tin.GetMonHocs.Add(new_thong_tin_mon_hoc);
                                    result_thong_tin.Add(new_thong_tin);
                                    Console.WriteLine("Add 1 item khoi");
                                    crawing_row++;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Het 1 khoi");
                                crawing_row = 3;
                                flag_mon = true;
                                Console.WriteLine("Ngat Vong Lap");
                            }
                        }
                        crawling_col = crawling_col + jump;
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
            File.WriteAllText(@"C:\path.json", Newtonsoft.Json.JsonConvert.SerializeObject(result_thong_tin));
            var query = from q in result_thong_tin
                        group q by q.GetKhoi into qs
                        select new
                        {
                            Khoi = qs.Key,
                            ThongTinMon = qs.SelectMany(r => r.GetMonHocs).OrderBy(r => r.GetMonHoc.MucDoUuTien),
                        };
            File.WriteAllText(@"C:\path.json", Newtonsoft.Json.JsonConvert.SerializeObject(query));
            return result_thong_tin;
        }
    }
    public class MonHoc
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public string MaMau { get; set; }
        public double SoTietHoc { get; set; }
        public int MucDoUuTien { get; set; }
    }
    public class ThongTinMonHoc
    {
        public MonHoc GetMonHoc { get; set; }
        public double SoTiet { get; set; }
    }
    public class KhoiLop
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public int SoLop { get; set; } = 5;
    }

    public class ThongTin
    {
        public KhoiLop GetKhoi { get; set; } = new KhoiLop();
        public List<ThongTinMonHoc> GetMonHocs { get; set; } = new List<ThongTinMonHoc>();
    }

}
