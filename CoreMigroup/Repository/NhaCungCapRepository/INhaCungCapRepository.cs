﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Repository.SanPhamRepository;
using CoreMigroup.Transactions;
using CoreMigroup.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.NhaCungCapRepository
{
    public interface INhaCungCapRepository : IBaseRepository<NhaCungCap>
    {
        Task<NhaCungCapPaging> GetListNhaCungCapByDichVuIdAsync(p_GetNhaCungCapByDichVuId request);
        Task<NhaCungCap> AddOrEdit_NCC(param_AddOrUpdateNCC request);
        Task<NhaCungCap> SaveNhaCungCap(param_SaveNhaCungCap request);
    }

    public class NhaCungCapRepository : BaseRepository<NhaCungCap>, INhaCungCapRepository
    {
        private ApplicationDbContext _db;
        private DbSet<NhaCungCap> _dbSet;
        private IUserHelper _userHelper;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        private readonly IDichVuRepository _dichVuRepository;
        private readonly ISanPhamRepository _sanPhamRepository;

        private UnitOfWorks _unitOfWorks;
        public NhaCungCapRepository(ApplicationDbContext db, IUserHelper userHelper, IWebHostEnvironment env, IConfiguration config, IDichVuRepository dichVuRepository, ISanPhamRepository sanPhamRepository) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<NhaCungCap>();
            _userHelper = userHelper;
            _env = env;
            _config = config;
            _unitOfWorks = new UnitOfWorks(db, userHelper);
            _dichVuRepository = dichVuRepository;
            _sanPhamRepository = sanPhamRepository;

        }

        public async Task<NhaCungCapPaging> GetListNhaCungCapByDichVuIdAsync(p_GetNhaCungCapByDichVuId request)
        {
            
            var result = new NhaCungCapPaging();
            if (request != null)
            {
                request.Keyword = request.Keyword ?? "";
                var lsNhaCungCap_Id = from sp in _unitOfWorks.BaseRepository<SanPham>().GetAll()
                                      where sp.DichVuId == request.dichVuId
                                      group sp.NhaCungCapId by sp.NhaCungCapId into g
                                      select g.Key;
                var lsNhaCungCap = from ncc in  _unitOfWorks.BaseRepository<NhaCungCap>().GetAll()
                                   join lsid in lsNhaCungCap_Id on ncc.Id equals lsid
                                   where ncc.Ma.ToLower().Contains(request.Keyword.ToLower()) || ncc.Ten.ToLower().Contains(request.Keyword.ToLower()) || ncc.TenTaiKhoan.ToLower().Contains(request.Keyword.ToLower())
                                   select ncc;
                var total = lsNhaCungCap.Count();
                lsNhaCungCap = lsNhaCungCap.Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);
                
                result.nhaCungCaps = await lsNhaCungCap.ToListAsync();
                result.total = total;
                return result;     
            }
            return result;
        }

        public async Task<NhaCungCap> AddOrEdit_NCC(param_AddOrUpdateNCC request)
        {
            if (request != null)
            {
                var ncc = new NhaCungCap();
                var r = new NhaCungCap();

                ncc.Id = request.id;
                ncc.Ma = request.MaNCC;
                ncc.Ten = request.TenNCC;
                ncc.SoDienThoai = request.Sdt;
                ncc.Email = request.Email;
                ncc.QuocGia = request.QuocGia;
                ncc.DiaChi = request.DiaChi;
                ncc.TenTaiKhoan = request.TenTK;
                ncc.SoTaiKhoan = request.SoTK;
                ncc.NganHang = request.TenNganHang;

                if(request.id == 0)
                {
                    r = await Insert(ncc);
                    return r;
                }
                else
                {
                    r = await Update(ncc);
                    return r;
                }
            }
            return null;
        }

        public async Task<NhaCungCap> SaveNhaCungCap(param_SaveNhaCungCap request)
        {
            var result = new NhaCungCap();
            if(request != null)
            {
                if(request.nhaCungCap != null)
                {
                    var out_ncc_id = 0;
                    if(request.nhaCungCap.Id <= 0)
                    {
                        var r = await Insert(request.nhaCungCap);
                        if (r.Id > 0)
                            out_ncc_id = r.Id;

                    }
                    else
                    {
                        var r = await Update(request.nhaCungCap);
                        if (r.Id > 0)
                            out_ncc_id = r.Id;
                    }
                    //add sp
                    if(out_ncc_id > 0)
                    {
                        //bat dau them san pham o day
                        var ls_san_pham = request.sanPhams;
                        //truong hop 1: chi insert
                        //truogn hop 2: chi update
                        //truong hop 3: xoa + update 1
                        //truong hop 4: xoa + update + insert

                        var all_sp = _unitOfWorks.BaseRepository<SanPham>().GetAll();

                        var cac_san_pham_update = from s in all_sp.AsEnumerable() 
                                                  join u in ls_san_pham on s.Id equals u.Id
                                                  select u;
                        cac_san_pham_update = cac_san_pham_update.ToList();
                        var cac_san_pham_insert = from u in ls_san_pham
                                                  where u.Id == 0
                                                  select u;
                        cac_san_pham_insert = cac_san_pham_insert.ToList();
                        var cac_san_pham_delete = from s in all_sp.AsEnumerable()
                                                  join u in cac_san_pham_update on new { s.DichVuId, s.NhaCungCapId } equals new { u.DichVuId, u.NhaCungCapId }
                                                  where s.Id != u.Id 
                                                  select s;
                        var lsIdUpdate = new List<int>();
                        foreach(var item in cac_san_pham_update)
                        {
                            lsIdUpdate.Add(item.Id);
                        }
                        cac_san_pham_delete = cac_san_pham_delete.Where(r => !lsIdUpdate.Contains(r.Id)).ToList();
                        //Insert nhung thang duoc insert
                        foreach (var item in cac_san_pham_insert)
                        {
                           item.NhaCungCapId = out_ncc_id;
                           var sp_inserted = await _unitOfWorks.BaseRepository<SanPham>().Insert(item);
                        }
                        foreach (var item in cac_san_pham_update)
                        {
                            var sp_updated = await _unitOfWorks.BaseRepository<SanPham>().Update(item);
                        }
                        foreach (var item in cac_san_pham_delete)
                        {
                            var sp_xoad = await _unitOfWorks.BaseRepository<SanPham>().Delete(item);
                        }
                        await _unitOfWorks.SaveChangeAsync();
                        

                    }

                    result = request.nhaCungCap;
                }
            }
            return result;
        }
    }
}
