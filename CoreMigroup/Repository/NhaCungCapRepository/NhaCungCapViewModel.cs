﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.NhaCungCapRepository
{
    #region params
    public class p_GetNhaCungCapByDichVuId : QueryParameters
    {
        public int dichVuId { get; set; }
    }
    public class p_GetNhaCungCapById
    {
        public int id { get; set; }
    }
    public class param_SaveNhaCungCap
    {
        public NhaCungCap nhaCungCap { get; set; }
        public List<SanPham> sanPhams { get; set; }
    }
    public class param_AddOrUpdateNCC : QueryParameters
    {
        public int id { get; set; } = 0;
        public string MaNCC { get; set; } = "";
        public string TenNCC { get; set; } = "";
        public string Sdt { get; set; } = "";
        public string Email { get; set; } = "";
        public string QuocGia { get; set; } = "";
        public string DiaChi { get; set; } = "";
        public string TenTK { get; set; } = "";
        public string SoTK { get; set; } = "";
        public string TenNganHang { get; set; } = "";
    }
    public class param_GetListNCC : QueryParameters
    {

    }
    #endregion
    public class NhaCungCapViewModel
    {
        
    }
}
