﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.FolderRepository
{
    public interface IFolderRepository : IBaseRepository<Folder>
    {
    }

    public class FolderRepository : BaseRepository<Folder>, IFolderRepository
    {
        private ApplicationDbContext _db;
        private DbSet<Folder> _dbSet;
        private IUserHelper _userHelper;
        public FolderRepository(ApplicationDbContext db, IUserHelper userHelper) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<Folder>();
            _userHelper = userHelper;

        }
    }
}
