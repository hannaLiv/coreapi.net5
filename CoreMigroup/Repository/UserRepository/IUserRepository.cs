﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using MemoryCaching;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.UserRepository
{
    public interface IUserRepository
    {
        List<ApplicationUser> GetList(param_FilterUser request, out int total);
        ApplicationUser GetByIdUser(int id);
        Task<string> ChangeAvatarAsync(string url_image);
        Dictionary<int, string> GetUserByLoaiUser(int loai);
        Task<bool> SaveNotificationTokens(string token, int user_id);
    }
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext _db;
        private IUserHelper _userHepler;
        private ICacheController _cacheController;
        public UserRepository(ApplicationDbContext db, IUserHelper userHelper, ICacheController cacheController)
        {
            _db = db;
            _userHepler = userHelper;
            _cacheController = cacheController;
        }
        public List<ApplicationUser> GetList(param_FilterUser request, out int total)
        {
            var r = _db.Users.AsNoTracking();
            if (!string.IsNullOrEmpty(request.Keyword))
                r = r.Where(x => x.UserName.Contains(request.Keyword) == true || x.TenDayDu.Contains(request.Keyword) == true);
            total = r.Count();
            if (request.Index > 0 && request.Size > 0)
                r = r.OrderByDescending(r => r.Id).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);

            return r.ToList();
        }

        public ApplicationUser GetByIdUser(int userid)
        {
            if (userid >= 0)
                return _db.Users.SingleOrDefault(x => x.Id == userid);
            return null;

        }

        public async Task<string> ChangeAvatarAsync(string url_image)
        {
            var result = "";
            var id = _userHepler.GetCurrentUserAsync().Result.Id;
            var user = _db.Users.Find(id);
            if (user != null)
            {
                user.ImagePath = url_image;
                try
                {
                    await _db.SaveChangesAsync();
                    result = url_image;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            return result;

        }

        public Dictionary<int, string> GetUserByLoaiUser(int loai)
        {
            var result = new Dictionary<int, string>();
            var key_cache = string.Format("GetUserByLoaiUser_{0}", loai);
            var cache_result = _cacheController.Get(key_cache);
            if (loai > 0)
            {
                if (string.IsNullOrEmpty(cache_result))
                {
                    var lsUser = _db.Users.Where(r => r.LoaiUser == loai);
                    foreach (var item in lsUser)
                    {
                        result.Add(item.Id, item.TenDayDu);
                    }
                }
            }
            else
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, string>>(cache_result);
            }

            return result;
        }

        public async Task<bool> SaveNotificationTokens(string token, int user_id)
        {
            if (user_id > 0)
            {
                var affected = new ApplicationUser();
                var key_cache = string.Format("GetUserDetailById_{0}", user_id);
                var cached = _cacheController.Get(key_cache);
                if (cached != null)
                {
                    affected = Newtonsoft.Json.JsonConvert.DeserializeObject<ApplicationUser>(cached);
                }
                else
                {
                    affected = _db.Users.Find(user_id);
                    if(affected != null)
                    {
                        _cacheController.Create(key_cache, Newtonsoft.Json.JsonConvert.SerializeObject(affected));
                    }
                }
                if (affected != null)
                {
                    var not = affected.NotificationTokens ?? "";
                    var notifications_tokens = not.Split(';').ToList();
                    if (!string.IsNullOrEmpty(token))
                    {
                        if (notifications_tokens.Where(r => r.Equals(token)).Count() <= 0)
                        {
                            notifications_tokens.Add(token);
                            var res = string.Join(";", notifications_tokens);
                            await _db.SaveChangesAsync();
                        }
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
