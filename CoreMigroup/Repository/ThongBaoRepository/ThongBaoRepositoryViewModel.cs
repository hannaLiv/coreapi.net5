﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.ThongBaoRepository
{
    public class ThongBaoRepositoryViewModel
    {
    }


    public class param_CreateThongBao
    {
        public string NoiDungThongBao { get; set; } = "";
        public int IdNguoiGui { get; set; } = 0;
        public bool IsHeThong { get; set; } = false;
        public bool IsGuiAll { get; set; } = false;
        public List<int> IdsNguoiNhan { get; set; } = new List<int>();
        public int Loai { get; set; }
    }

    public class param_UpdateThongBaoDaXem
    {
        public List<int> lsId { get; set; } = new List<int>();
    }
}
