﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.ThongBaoRepository
{
    public interface IThongBaoRepository : IBaseRepository<ThongBao>
    {
        string Test();
        Task<ThongBao> CreateThongBao(param_CreateThongBao request);
        Task<bool> UpdateDaXemThongBao(List<int> lsId);
        
    }
    public class ThongBaoRepository : BaseRepository<ThongBao>, IThongBaoRepository
    {
        private ApplicationDbContext _db;
        private DbSet<ThongBao> _dbSet;
        private IUserHelper _userHelper;
        public ThongBaoRepository(ApplicationDbContext db, IUserHelper userHelper) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<ThongBao>();
            _userHelper = userHelper;

        }

        public async Task<ThongBao> CreateThongBao(param_CreateThongBao request)
        {
            var thongBao = new ThongBao();
            if (request != null)
            {
                
                thongBao.IdNguoiGui = request.IdNguoiGui;
                thongBao.Loai = request.Loai;
                thongBao.IsGuiAll = request.IsGuiAll;
                thongBao.IsHeThongGui = request.IsHeThong;

                try
                {
                    _dbSet.Add(thongBao);
                    _db.SaveChanges();
                    var outId = thongBao.Id;
                    var lsUserNhan = request.IdsNguoiNhan;
                    var x = new List<ChiTietThongBao>();
                    foreach(var item in lsUserNhan)
                    {
                        var y = new ChiTietThongBao();
                        y.IdNguoiNhan = item;
                        y.NoiDungThongBao = request.NoiDungThongBao;
                        y.IdThongBao = outId;
                        x.Add(y);
                    }
                    _db.ChiTietThongBaos.AddRange(x);
                    await _db.SaveChangesAsync();
                }
                catch (Exception ex)
                {

                    throw;
                }
                
            }
            return thongBao;


        }

        public string Test()
        {
            return "Toi la ham test";

        }

        public async Task<bool> UpdateDaXemThongBao(List<int> lsId)
        {
            if(lsId.Count > 0)
            {
                var str = string.Join(",", lsId);
                var finder = _db.ChiTietThongBaos.Where(r => str.Contains(r.Id.ToString()));
                foreach(var item in finder)
                {
                    item.IsDaXem = true;
                }
                _db.ChiTietThongBaos.UpdateRange(finder);
                try
                {
                    await _db.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    //throw;
                }
                
            }
            return false;
        }
    }
}
