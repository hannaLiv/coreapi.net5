﻿using CoreMigroup.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.SanPhamRepository
{
    public class SanPhamViewModel
    {
    }
    #region params
    public class param_AddOrUpdate_SP : QueryParameters 
    {
        public List<int> id { get; set; } = new List<int>();
        public int IdNCC { get; set; } = 0;
        public int IdDV { get; set; } = 0;
        public List<string> Ten { get; set; } = new List<string>();
        public List<double> GiaBan { get; set; } = new List<double>();
        public List<double> GiaNhap { get; set; } = new List<double>();
        public List<double> GiaKhuyenMai { get; set; } = new List<double>();
        public List<int> DonViTinh { get; set; } = new List<int>();
        public List<string> GhiChu { get; set; } = new List<string>();

    }

    #endregion
}
