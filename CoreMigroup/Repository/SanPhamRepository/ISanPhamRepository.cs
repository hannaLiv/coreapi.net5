﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.SanPhamRepository
{
    public interface ISanPhamRepository : IBaseRepository<SanPham>
    {
        Task<SanPham> AddOrEdit_SP(param_AddOrUpdate_SP request);
        Task<SanPham> DeleteById(int id);
    }
    public class SanPhamRepository : BaseRepository<SanPham>, ISanPhamRepository
    {
        private ApplicationDbContext _db;
        private DbSet<SanPham> _dbSet;
        private IUserHelper _userHelper;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        private readonly IDichVuRepository _dichVuRepository;


        private UnitOfWorks _unitOfWorks;
        public SanPhamRepository(ApplicationDbContext db, IUserHelper userHelper, IWebHostEnvironment env, IConfiguration config, IDichVuRepository dichVuRepository) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<SanPham>();
            _userHelper = userHelper;
            _env = env;
            _config = config;
            _unitOfWorks = new UnitOfWorks(db, userHelper);
            _dichVuRepository = dichVuRepository;

        }

        public async Task<SanPham> AddOrEdit_SP(param_AddOrUpdate_SP request)
        {
            
            if(request != null)
            {
                if(request.Ten.Count() > 0)
                {
                    
                    for(int i =0; i< request.Ten.Count(); i++)
                    {
                        var sp = new SanPham();
                        var r = new SanPham();
                        sp.NhaCungCapId = request.IdNCC;
                        sp.DichVuId = request.IdDV;
                        sp.Ten = request.Ten[i];
                        sp.GiaBan = request.GiaBan[i];
                        sp.GiaNhap = request.GiaNhap[i];
                        sp.GiaKhuyenMai = request.GiaKhuyenMai[i];
                        sp.DonViTinh = request.DonViTinh[i];
                        sp.GhiChu = request.GhiChu[i];
                        if (request.id[i] == 0)
                        {
                            r = await Insert(sp);
                        }
                        else
                        {
                            sp.Id = request.id[i];

                            r = await Update(sp);
                        }
                    }
                }
            }
            return null;
        }

        public async Task<SanPham> DeleteById(int id)
        {
            if(id > 0)
            {
                var sp = new SanPham();
                var r = new SanPham();
                sp.Id = id;
                r = await Delete(sp);
            }
            return null;
        }
    }
}
