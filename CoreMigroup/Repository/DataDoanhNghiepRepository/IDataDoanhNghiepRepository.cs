﻿using CoreMigroup.Data;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CoreMigroup.Repository.DataDoanhNghiepRepository
{
    public interface IDataDoanhNghiepRepository
    {
        List<DataDoanhNghiep> GetListDataDN(param_GetDataDN request, out int total);
    }
    public class DataDoanhNghiepRepository : IDataDoanhNghiepRepository
    {
        private ApplicationDbContext _db;
        public DataDoanhNghiepRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public List<DataDoanhNghiep> GetListDataDN(param_GetDataDN request, out int total)
        {
            var r = _db.DataDoanhNghieps.AsNoTracking();
            if (!string.IsNullOrEmpty(request.Keyword))
                r = r.Where(x => x.MaSoThue.Contains(request.Keyword) || x.TenCongTy.Contains(request.Keyword) );
            total = r.Count();
            if (request.Index > 0 && request.Size > 0)
                r = r.OrderByDescending(r => r.Id).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);

            return r.ToList();
        }

        public ApplicationUser GetByIdUser(int userid)
        {
            if (userid >= 0)
                return _db.Users.SingleOrDefault(x => x.Id == userid);
            return null;

        }
    }
}
