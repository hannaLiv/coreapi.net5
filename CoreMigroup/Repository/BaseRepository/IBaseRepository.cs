﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Transactions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.BaseRepository
{
    public interface IBaseRepository<T> where T : Base
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetByCondition(Expression<Func<T, bool>> expression , out int total, bool isFildDelete = false, int index = 1, int size = 10);
        IQueryable<T> GetAllPaging(int index, int size, out int total);

        IQueryable<T> GetByKeyword(string keyword, int index, int size, out int total);

        T GetById(int id);
        Task<T> Insert(T entity);
        Task<T> Update(T entity);

        Task<T> Delete(T entity);

        Task SaveChangesAsync();


    }

    public class BaseRepository<T> : IBaseRepository<T> where T : Base
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<T> _dbSet;
        private readonly IUserHelper _userHelper;
        private readonly UnitOfWorks _unitOfWorks;

        public BaseRepository(ApplicationDbContext db, IUserHelper userHelper)
        {
            _db = db;
            _dbSet = db.Set<T>();
            _userHelper = userHelper;
            _unitOfWorks = new UnitOfWorks(db);
        }

        public async Task<T> Delete(T entity)
        {
            if(entity != null)
            {
                var r = _dbSet.Find(entity.Id);
                r.IsDaXoa = true;
                r.IdNguoiXoa = _userHelper.GetCurrentUserAsync().Result.Id;
                r.NgayXoa = DateTime.Now;
                _dbSet.Update(r);
                await SaveChangesAsync();
            }
            return entity;
        }

        public IQueryable<T> GetAll()
        {
            var result = _dbSet.Where(r => r.IsDaXoa != false).AsNoTracking();
            
            return result;
        }


        public IQueryable<T> GetAllPaging(int index, int size, out int total)
        {
            total = 0;
            
            if(index >0 && size > 0)
            {
                var result = _dbSet.Skip((index - 1) * size).Take(index * size).AsQueryable();
                total = result.Count();
                return result;
            }
            return null;
        }

        public IQueryable<T> GetByCondition(Expression<Func<T, bool>> expression, out int total, bool isFildDelete = false, int index = 1, int size = 10)
        {
            total = 0;
            var r = _dbSet.Where(expression);

            if (!isFildDelete)
                r = r.Where(x => x.IsDaXoa == false);
            if (index > 0 && size > 0)
            {
                r = r.Skip((index - 1) * size).Take(size);
            }
            total = r.Count();
            return r.AsNoTracking();
        }

        public T GetById(int id)
        {
            if(id >= 0)
                return _dbSet.Find(id);
            return null;
        }

        public IQueryable<T> GetByKeyword(string keyword, int index, int size, out int total)
        {
            total = 0;
            if (keyword != null)
            {
                if (index > 0 && size > 0)
                {
                    var result = _dbSet.Skip((index - 1) * size).Take(index * size).AsQueryable();
                    total = result.Count();
                    return result;
                }
                return null;
            }
            return null;
        }

        public async Task<T> Insert(T entity)
        {
            if(entity != null)
            {
                entity.IdNguoiTao = _userHelper.GetCurrentUserAsync().Result.Id;
                entity.NgayTao = DateTime.Now;
                _dbSet.Add(entity);
                await SaveChangesAsync();
                return entity;
            }
            return null;
            
        }

        public async Task SaveChangesAsync()
        {
            await _unitOfWorks.SaveChangeAsync();
        }

        public async Task<T> Update(T entity)
        {
            if(entity != null)
            {
                entity.IdNguoiCapNhat = _userHelper.GetCurrentUserAsync().Result.Id;
                entity.NgayCapNhat = DateTime.Now;
                _dbSet.Update(entity);
                await SaveChangesAsync();
                return entity;
            }
            return null;
        }
    }
}
