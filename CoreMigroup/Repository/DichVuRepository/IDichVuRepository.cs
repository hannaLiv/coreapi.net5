﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.DichVuRepository
{
    public interface IDichVuRepository : IBaseRepository<DichVu>
    {
        Task<DichVu> AddOrEdit_DichVu(param_DichVu request);
        Task<DichVu> Delete_DichVu(int id);
    }
    public class DichVuRepository : BaseRepository<DichVu>, IDichVuRepository
    {
        private ApplicationDbContext _db;
        private DbSet<DichVu> _dbSet;
        private IUserHelper _userHelper;
        public DichVuRepository(ApplicationDbContext db, IUserHelper userHelper) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<DichVu>();
            _userHelper = userHelper;
        }

        public async Task<DichVu> AddOrEdit_DichVu(param_DichVu request)
        {
            if(request != null)
            {
                var dichVu = new DichVu();
                var re = new DichVu();
                dichVu.Id = request.Id;
                dichVu.Ten = request.Ten;
                
                if(request.Id == 0)
                {
                    re = await Insert(dichVu);
                    return re;
                }
                else
                {
                    re = await Update(dichVu);
                    return re;
                }
                
            }
            return null;
        }

        public async Task<DichVu> Delete_DichVu(int id)
        {
            if(id != 0)
            {
                var d = new DichVu();
                var res = new DichVu();
                d.Id = id;
                res = await Delete(d);
                return res;
            }
            return null;
        }
    }
}
