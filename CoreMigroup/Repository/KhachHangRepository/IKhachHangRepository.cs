﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using MemoryCaching;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.KhachHangRepository
{
    public interface IKhachHangRepository  : IBaseRepository<KhachHang>
    {
        Task<List<string>> GetDanhMucKhachHangAsync();
        Task<List<string>> GetNguonKhachHangAsync();
        Task<KhachHang> AddOrEdit_KhachHang(param_KhachHang request);
    }

    public class KhachHangReposiotry : BaseRepository<KhachHang>, IKhachHangRepository
    {
        private ApplicationDbContext _db;
        private DbSet<KhachHang> _dbSet;
        private IUserHelper _userHelper;
        private ICacheController _cacheController;
        public KhachHangReposiotry(ApplicationDbContext db, IUserHelper userHelper, ICacheController cacheController) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<KhachHang>();
            _userHelper = userHelper;
            _cacheController = cacheController;

        }

        public async Task<KhachHang> AddOrEdit_KhachHang(param_KhachHang request)
        {
            if(request != null)
            {
                var khachHang = new KhachHang();
                var re = new KhachHang();
                khachHang.Id = request.id;
                khachHang.Ten = request.Ten;
                khachHang.GioiTinh = request.GioiTinh;
                khachHang.NgaySinh = request.NgaySing;
                khachHang.DiaChi = request.DiaChi;
                khachHang.Email = request.Email;
                khachHang.SoCMND = request.CMND;
                khachHang.SoDienThoai = request.SoDT;
                khachHang.DanhMucKhachHang = request.LoaiKH;
                khachHang.NguonKhachHang = request.NguonKH;
                khachHang.NhanVienPhuTrach = request.NhanVienPTId;

                if (request.id == 0)
                {
                    re = await Insert(khachHang);
                    _cacheController.Remove("GetDanhMucKhachHang_v1");
                    _cacheController.Remove("GetNguonKhachHang_v1");
                    return re;
                }
                else
                {
                    re = await Update(khachHang);
                    _cacheController.Remove("GetDanhMucKhachHang_v1");
                    _cacheController.Remove("GetNguonKhachHang_v1");
                }
            }
            return null;

        }

        public async Task<List<string>> GetDanhMucKhachHangAsync()
        {
            var result = new List<string>();
            //Key cache
            var key_cache = "GetDanhMucKhachHang_v1";
            var cache_result = _cacheController.Get(key_cache);
            if (string.IsNullOrEmpty(cache_result))
            {
                var query = from d in _dbSet
                            where !string.IsNullOrEmpty(d.DanhMucKhachHang)
                            group d by d.DanhMucKhachHang into g
                            select g.Key;
                result = await query.ToListAsync();
                //Luwu laiu cai cache nua
                _cacheController.Create(key_cache, Newtonsoft.Json.JsonConvert.SerializeObject(result));
            }
            else
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(cache_result);
            }
            return result;
        }

        public async Task<List<string>> GetNguonKhachHangAsync()
        {
            var result = new List<string>();
            //Key cache
            var key_cache = "GetNguonKhachHang_v1";
            var cache_result = _cacheController.Get(key_cache);
            if (string.IsNullOrEmpty(cache_result))
            {
                var query = from d in _dbSet
                            where !string.IsNullOrEmpty(d.NguonKhachHang)
                            group d by d.NguonKhachHang into g
                            select g.Key;
                result = await query.ToListAsync();
                //Luwu laiu cai cache nua
                _cacheController.Create(key_cache, Newtonsoft.Json.JsonConvert.SerializeObject(result));
            }
            else
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(cache_result);
            }
            //Kiem tra cache

            return result;
        }

        //Lenh o day


    }


}
