﻿using CoreMigroup.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.KhachHangRepository
{
    public class KhachHangRepositoryViewModel
    {
    }
    #region Parameters
    public class param_KhachHang
    {
        public int id { get; set; } = 0;
        public int NhanVienPTId { get; set; } = 0;
        public DateTime NgaySing { get; set; } = DateTime.Now;
        public string Ten { get; set; } = "";
        public int GioiTinh { get; set; } = 0;
        public string DiaChi { get; set; } = "";
        public string Email { get; set; } = "";
        public string SoDT { get; set; } = "";
        public string CMND { get; set; } = "";
        public string LoaiKH { get; set; } = "";
        public string NguonKH { get; set; } = "";
    }

    public class param_GetlistKhachHang: QueryParameters
    {
        public List<string> DanhMuc { get; set; } = new List<string>();
        public List<string> Nguon { get; set; } = new List<string>();
        public DateTime? NgayUploadTu { get; set; }
        public DateTime? NgayUploadDen { get; set; }
    }
    #endregion
}
