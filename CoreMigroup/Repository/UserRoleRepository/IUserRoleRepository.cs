﻿using CoreMigroup.Data;
using CoreMigroup.Enum;
using CoreMigroup.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.UserRoleRepository
{
    public interface IUserRoleRepository
    {
        bool CheckRoleByRoleNameAndUserId(int userId, string role);
        bool SaveQuyen(param_SaveQuyen request);
        List<int> GetAllRoleIdsByUserId(int userId);
    }
    public class UserRoleRepository : IUserRoleRepository
    {
        private ApplicationDbContext _db;
        public UserRoleRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public bool CheckRoleByRoleNameAndUserId(int userId, string role)
        {
            if(userId > 0 && !string.IsNullOrEmpty(role))
            {
                var currentUserRoles = _db.UserRoles.Where(r => r.UserId == userId);
                var isAdmin = currentUserRoles.Where(r => r.RoleId == (int)TenRoleEnum.Admin);
                if (isAdmin.Count() > 0)
                    return true;
                else
                {
                    var join_query = from t1 in currentUserRoles
                                     join t2 in _db.Roles on t1.RoleId equals t2.Id
                                     select new
                                     {
                                         IdRole = t1.RoleId,
                                         TenRole = t2.Name
                                     };
                    var checker = join_query.Where(r => r.TenRole.Equals(role)).FirstOrDefault();
                    if (checker != null)
                        return true;

                }
            }
            return false;
            

        }

        public List<int> GetAllRoleIdsByUserId(int userId)
        {
            var result = new List<int>();
            if(userId > 0)
            {
                var b = _db.UserRoles.Where(r => r.UserId == userId);
                if(b != null)
                {
                    foreach(var item in b)
                    {
                        result.Add(item.RoleId);
                    }
                }
            }
            return result;
        }

        public bool SaveQuyen(param_SaveQuyen request)
        {
            if(request != null)
            {
                var userId = request.UserId;
                var old = _db.UserRoles.Where(r => r.UserId == userId);
                var _new = new List<IdentityUserRole<int>>();
                _db.UserRoles.RemoveRange(old);
                foreach(var item in request.RoleIds)
                {
                    var i = new IdentityUserRole<int>();
                    i.UserId = userId;
                    i.RoleId = item;
                    _new.Add(i);
                }
                _db.UserRoles.AddRange(_new);
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
