﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.MenuRepository
{
    public interface IMenuRepository : IBaseRepository<Menu>
    {
    }

    public class MenuRepository : BaseRepository<Menu>, IMenuRepository
    {
        private ApplicationDbContext _db;
        private DbSet<Menu> _dbSet;
        private IUserHelper _userHelper;
        public MenuRepository(ApplicationDbContext db, IUserHelper userHelper) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<Menu>();
            _userHelper = userHelper;

        }
    }

}
