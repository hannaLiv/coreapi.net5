﻿using CoreMigroup.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.FileUploadRepository
{
    public class FileUploadViewModel
    {
    }

    #region Parameters
    public class param_GetFileUpload : QueryParameters
    {
        public List<int> FolderIds { get; set; } = new List<int>();
        public DateTime? NgayUploadTu { get; set; } 
        public DateTime? NgayUploadDen { get; set; }

    }

    public class param_SaveFileUpload
    {
        public int id { get; set; } = 0;
        public string tenFile { get; set; } = "";
        public string motaFile { get; set; } = "";
        public int danhMucFile { get; set; } = 0;
        public string danhMucTen { get; set; } = "";
        public List<IFormFile> lsFiles { get; set; } = new List<IFormFile>();
    }
    #endregion
}
