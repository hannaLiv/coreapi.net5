﻿using CoreMigroup.Data;
using CoreMigroup.Enum;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using CoreMigroup.Repository.FolderRepository;
using CoreMigroup.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.FileUploadRepository
{
    public interface IFileUploadRepository : IBaseRepository<FileUpload>
    {
        Task<FileUpload> AddOrUpdateFileUpload_ExcelDoanhNghiep(param_SaveFileUpload request);
    }
    public class FileUploadRepository : BaseRepository<FileUpload>, IFileUploadRepository
    {
        private ApplicationDbContext _db;
        private DbSet<FileUpload> _dbSet;
        private IUserHelper _userHelper;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        private readonly IFolderRepository _folderRepository;
        private UnitOfWorks _unitOfWorks;
        public FileUploadRepository(ApplicationDbContext db, IUserHelper userHelper, IWebHostEnvironment env, IConfiguration config, IFolderRepository folderRepository) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<FileUpload>();
            _userHelper = userHelper;
            _env = env;
            _config = config;
            _folderRepository = folderRepository;
            _unitOfWorks = new UnitOfWorks(db, userHelper);

        }

        public async Task<FileUpload> AddOrUpdateFileUpload_ExcelDoanhNghiep(param_SaveFileUpload request)
        {
            if(request != null)
            {
                //Tao folder
                var tenFolder = request.danhMucTen;
                var parentId = 0;
                if (!string.IsNullOrEmpty(tenFolder) && request.danhMucFile == 0)
                {
                    var checker = _folderRepository.GetAll().Where(r => r.Ten.Equals(tenFolder) && r.ParentId == parentId);
                    if (checker.Count() <= 0)
                    {
                        var f = new Folder();
                        f.Ten = tenFolder;
                        //f.ParentId = parentId == null ? null : parentId;
                        f.ParentId = null;
                        f.IsFolderCategory = true;
                        var webRootPath = _env.WebRootPath;
                        var user = _userHelper.GetCurrentUserAsync().Result.Id;
                        var path = string.Format("UserID_{0}\\{1}", user, tenFolder);
                        var save_path = path;
                        path = webRootPath + "\\Upload\\" + path;
                        var uploadBaseFolder = _config.GetValue<string>("FileSetting:UploadBaseFolder");
                        var path_save_database = "\\" + uploadBaseFolder + "\\" + save_path;
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        f.DuongDan = path_save_database;
                        var r = await _unitOfWorks.BaseRepository<Folder>().Insert(f);
                        //var r = await _folderRepository.Insert(f);
                        request.danhMucFile = r.Id;
                    }
                }
                //upload file

                if (request.id > 0 || (request.id == 0 && request.lsFiles.Count > 0))
                {
                    var allowTypeList = new List<string>();
                    var allowTypes = _config.GetValue<string>("FileSetting:AllowType");
                    if (!string.IsNullOrEmpty(allowTypes))
                    {
                        allowTypeList = allowTypes.Split(';').ToList();
                    }
                    var maxSize = _config.GetValue<long>("FileSetting:MaxSize");
                    var webRootPath = _env.WebRootPath;
                    //var webRootPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                    //var rp = _env.WebRootFileProvider.GetDirectoryContents(webRootPath);
                    //var mapPath = Path.GetPathRoot(webRootPath);
                    var l = request.lsFiles;
                    var fileInfo = new FileUpload();
                    var t = new FileUpload();
                    if (request.id > 0)
                    {
                        fileInfo = GetById(request.id);
                    }
                    fileInfo.Ten = request.tenFile;
                    fileInfo.MoTa = request.motaFile;
                    if (l.Count() > 0)
                    {
                        var item = l[0];
                        var fileExt = Path.GetExtension(item.FileName).ToLower();
                        var matchedExt = allowTypeList.Where(r => r.Trim() == fileExt).FirstOrDefault();
                        if (matchedExt != null && item.Length < maxSize)
                        {
                            var now = DateTime.Now;
                            var tick = now.Ticks;
                            var fileName = tick + "_" + item.FileName;
                            
                            fileInfo.FolderId = request.danhMucFile;
                            //get duong dan cua folder
                            var folderSeleced = _folderRepository.GetById(request.danhMucFile);
                            //Directory.CreateDirectory(Path.Combine(webRootPath,folderSeleced.DuongDan));
                            var folderDir = "";
                            if (folderSeleced != null)
                            {
                                folderDir = folderSeleced.DuongDan;
                            }
                            Directory.CreateDirectory(webRootPath + folderDir);
                            var duongDanFile = folderSeleced.DuongDan + "\\" + fileName;
                            var duongDanLuu = webRootPath + "\\" + duongDanFile;
                            fileInfo.DuongDan = duongDanFile;
                            fileInfo.KichThuoc = (int)item.Length;
                            
                            var loaiFile = 0;
                            var avatar = "";
                            switch (fileExt)
                            {
                                case ".doc":
                                    loaiFile = (int)LoaiFileEnum.DOC;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".docx":
                                    loaiFile = (int)LoaiFileEnum.DOC;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".xls":
                                    loaiFile = (int)LoaiFileEnum.Excel;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".xlsx":
                                    loaiFile = (int)LoaiFileEnum.Excel;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".jpg":
                                    loaiFile = (int)LoaiFileEnum.JPG;
                                    break;
                                case ".png":
                                    loaiFile = (int)LoaiFileEnum.PNG;
                                    break;
                                case ".svg":
                                    loaiFile = (int)LoaiFileEnum.SVG;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".pdf":
                                    loaiFile = (int)LoaiFileEnum.PDF;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".json":
                                    loaiFile = (int)LoaiFileEnum.Json;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".xml":
                                    loaiFile = (int)LoaiFileEnum.Xml;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                case ".html":
                                    loaiFile = (int)LoaiFileEnum.Html;
                                    avatar = "\\images\\thumbnail\\excel_thumb.png";
                                    break;
                                default:
                                    loaiFile = (int)LoaiFileEnum.KhongXacDinh;
                                    break;
                            }
                            fileInfo.DuoiFile = fileExt;
                            fileInfo.Loai = loaiFile;
                            fileInfo.Avatar = avatar;
                            fileInfo.Id = request.id;
                            


                            using (var stream = new FileStream(duongDanLuu, FileMode.Create))
                            {
                                //luu file vao database,

                                //luu file vao o cung ma toi chon
                                item.CopyTo(stream);
                            }
                        }
                    }
                    if (fileInfo.Id <= 0)
                    {
                        t = await _unitOfWorks.BaseRepository<FileUpload>().Insert(fileInfo);
                        //t = await Insert(fileInfo);
                        return t;
                    }
                    else
                    {
                        t = await _unitOfWorks.BaseRepository<FileUpload>().Update(fileInfo);
                        //t = await Update(fileInfo);
                        return t;
                    }
                }
            }
            return null;
        }
    }
}
