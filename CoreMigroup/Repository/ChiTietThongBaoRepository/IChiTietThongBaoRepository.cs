﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Repository.ChiTietThongBaoRepository
{
    public interface IChiTietThongBaoRepository : IBaseRepository<ChiTietThongBao>
    {
    }
    public class ChiTietThongBaoRepository : BaseRepository<ChiTietThongBao>, IChiTietThongBaoRepository
    {
        private ApplicationDbContext _db;
        private DbSet<ChiTietThongBao> _dbSet;
        private IUserHelper _userHelper;
        public ChiTietThongBaoRepository(ApplicationDbContext db, IUserHelper userHelper) : base(db, userHelper)
        {
            _db = db;
            _dbSet = db.Set<ChiTietThongBao>();
            _userHelper = userHelper;

        }
    }

}
