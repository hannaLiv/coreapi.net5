using CoreAPI.JWTHelper;
using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.BaseRepository;
using CoreMigroup.Repository.ChiTietThongBaoRepository;
using CoreMigroup.Repository.DataDoanhNghiepRepository;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Repository.FileUploadRepository;
using CoreMigroup.Repository.FolderRepository;
using CoreMigroup.Repository.KhachHangRepository;
using CoreMigroup.Repository.MenuRepository;
using CoreMigroup.Repository.NhaCungCapRepository;
using CoreMigroup.Repository.SanPhamRepository;
using CoreMigroup.Repository.ThongBaoRepository;
using CoreMigroup.Repository.UserRepository;
using CoreMigroup.Repository.UserRoleRepository;
using CoreMigroup.SignalRHub;
using MemoryCaching;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CoreMigroup
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDatabaseDeveloperPageExceptionFilter();

            #region Injection
            services.AddTransient<ApplicationDbContext>();
            services.AddScoped<IAuthenticateService, AuthenticateService>();
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ICacheController, CacheController>();
            services.AddScoped<DbContext, ApplicationDbContext>();
            services.AddScoped<IUserHelper, UserHelper>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IThongBaoRepository, ThongBaoRepository>();
            services.AddScoped<IChiTietThongBaoRepository, ChiTietThongBaoRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<IFileUploadRepository, FileUploadRepository>();
            services.AddScoped<IFolderRepository, FolderRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IThongBaoTaiKhoanMoi, ThongBaoTaiKhoanMoi>();
            services.AddScoped<ICrawlingExcelHelper, CrawlingExcelHelper>();
            services.AddScoped<IDataDoanhNghiepRepository, DataDoanhNghiepRepository>();
            services.AddScoped<IKhachHangRepository, KhachHangReposiotry>();
            services.AddScoped<IDichVuRepository, DichVuRepository>();
            services.AddScoped<INhaCungCapRepository, NhaCungCapRepository>();
            services.AddScoped<ISanPhamRepository, SanPhamRepository>();
            #endregion
            //services.AddAuthentication()
            //    .AddFacebook(config =>
            //    {
            //        config.AppId = Configuration["App:FaceClientId"];
            //        config.ClientSecret = Configuration["App:FaceClientSecret"];
            //    }
            //    )
            //    .AddGoogle(config => {

            //        config.ClientId = Configuration["App:GoogleClientId"];
            //        config.ClientSecret = Configuration["App:GoogleClientSecret"];

            //    });
            //services.AddDefaultIdentity<IdentityUser<int>>(options => options.SignIn.RequireConfirmedAccount = true)
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultUI()
            .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                // Default Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
            });
            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    options.ClientId = Configuration["Authentication:Google:ClientId"]; ;
                    options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                    //options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;

                })
                //.AddFacebook(otions =>
                //{
                //    otions.AppId = Configuration["App:FacebookClientId"];
                //    otions.ClientSecret = Configuration["App:FacebookClientSecret"];
                //})

                ;

            //services.AddMvc().AddRazorOptions(o => o.);
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
                options.OnAppendCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);


            });
            services.AddAuthentication();
            services.AddMemoryCache();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoreAPI", Version = "v1" });
            });
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddSignalR();
            //services.AddMvc().AddRazorOptions(options => options.AllowRecompilingViewsOnFileChange = true);


            //services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseMigrationsEndPoint();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                    c.RoutePrefix = "";
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            //app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            //app.UseFileServer(new FileServerOptions
            //{
            //    EnableDirectoryBrowsing = false
            //});


            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(env.WebRootPath, "AdminLTE-2.4.18")),
                RequestPath = "/AdminLTE-2.4.18",
                EnableDirectoryBrowsing = false
            });


            app.UseRouting();
            app.UseCors("MyPolicy");



            app.UseAuthentication();

            app.UseAuthorization();

            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<ChatHub>("/chatHub");
            //});
            app.UseMiddleware<JWTMiddleware>();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapHub<ThongBaoTaiKhoanMoi>("/thong-bao");
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller=Home}/{action=FirstLoad}/{id?}");
            //    endpoints.MapRazorPages();


            //});
            app.UseEndpoints(x => x.MapControllers());

        }
        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                // TODO: Use your User Agent library of choice here. 
                if (!string.IsNullOrEmpty(userAgent))
                {
                    options.SameSite = SameSiteMode.Unspecified;
                }
            }
        }

    }
}
