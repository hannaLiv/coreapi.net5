﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Enum
{
    public enum LoaiRoleEnum
    {
        Admin = 1,
        Menu = 2,
        DanhSachFile = 3,
        DanhSachUser = 4
    }
    public enum TenRoleEnum
    {
        Admin = 1,
        Menu = 2,
        DanhSachFile = 8,
        HienThiMenuFile = 6,
        HienThiMenuUser = 7,
        ChoPhepDownload = 9,
        ChoPhepUpload = 10,
        XemDanhSachFile=11,
        DanhSachUser = 12,
        ThemSuaXoaUser = 13,
        HienThiMenuDataDN=1012,
        HienThiMenuQuanLyHeThong = 1013,
        HienThiMenuNCC = 1014
    }
}
