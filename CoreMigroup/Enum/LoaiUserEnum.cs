﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Enum
{
    public enum LoaiUserEnum
    {
        SuperAdmin = 1,
        KhachHang = 2,
        NhanVien = 3
    }
}
