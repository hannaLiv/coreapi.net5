﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Enum
{
    public enum LoaiFileEnum
    {
        KhongXacDinh = 1,
        Excel = 2,
        JPG = 3,
        PNG = 4,
        SVG = 5,
        PDF = 6,
        DOC = 7,
        Json = 8,
        Xml = 9,
        Html = 10
    }
}
