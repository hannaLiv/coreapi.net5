﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Enum
{
    public enum LoaiGioiTinhEnum
    {
        KhongXacDinh = 0,
        Nam = 1,
        Nu = 2,
        BD = 3
    }
}
