﻿using CoreMigroup.Data;
using CoreMigroup.Helper;
using CoreMigroup.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Transactions
{
    public class UnitOfWorks : IDisposable
    {
        private readonly ApplicationDbContext context;
        private bool disposed;
        private Dictionary<string, object> repositories;
        private IUserHelper _userHelper;
        

        public UnitOfWorks(ApplicationDbContext context)
        {
            this.context = context;
        }
        public UnitOfWorks(ApplicationDbContext context, IUserHelper userHelper)
        {
            this.context = context;
            _userHelper = userHelper;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            context.SaveChanges();
        }
        public async Task SaveChangeAsync()
        {
            await context.SaveChangesAsync();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public BaseRepository<T> BaseRepository<T>() where T : Models.Base
        {
            if (repositories == null)
            {
                repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (!repositories.ContainsKey(type))
            {
                var repositoryType = typeof(BaseRepository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context, _userHelper);
                repositories.Add(type, repositoryInstance);
            }
            return (BaseRepository<T>)repositories[type];
        }
    }
}
