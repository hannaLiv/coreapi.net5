﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class Folder : Base
    {
        public string DuongDan { get; set; } = "";
        public int? ParentId { get; set; } = null;
        public bool IsFolderCategory { get; set; } = false;

        [ForeignKey("ParentId")]
        public Folder GetFolder { get; set; }
    }
}
