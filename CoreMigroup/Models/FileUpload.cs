﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class FileUpload : Base
    {
        public int FolderId { get; set; } = 0;
        public string DuongDan { get; set; } = "";
        public int KichThuoc { get; set; } = 0;
        public int SoLuotTai { get; set; } = 0;
        public int SoLuotXem { get; set; } = 0;
        public string DuoiFile { get; set; } = "";
        public string Avatar { get; set; } = "";


        [ForeignKey("FolderId")]
        public virtual Folder GetFolder { get; set; }

    }
}
