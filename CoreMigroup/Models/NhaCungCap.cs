﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class NhaCungCap : Base
    {
        public int ParentId { get; set; } = 0;
        public string SoDienThoai { get; set; } = "";
        public string Email { get; set; } = "";
        public string QuocGia { get; set; } = "";
        public string DiaChi { get; set; } = "";
        public string TenTaiKhoan { get; set; } = "";
        public string SoTaiKhoan { get; set; } = "";
        public string NganHang { get; set; } = "";
    }
}
