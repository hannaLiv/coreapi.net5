﻿using CoreMigroup.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class Base
    {
        
        [Key]
        public int Id { get; set; }
        public string Ma { get; set; } = "";
        public string Ten { get; set; } = "";
        public string MoTa { get; set; } = "";
        public int Loai { get; set; } = 1;
        public string MetaData { get; set; } = "";
        public int TrangThai { get; set; } = 1;
        public string GhiChu { get; set; } = "";
        public int? IdNguoiTao { get; set; } = 1;
        public DateTime? NgayTao { get; set; } = DateTime.Now;
        public int? IdNguoiCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public bool? IsDaXoa { get; set; }
        public int? IdNguoiXoa { get; set; }
        public DateTime? NgayXoa { get; set; }
        
        [ForeignKey("IdNguoiTao")]
        public ApplicationUser GetNguoiTao { get; set; }
        [ForeignKey("IdNguoiCapNhat")]
        public ApplicationUser GetNguoiCapNhat { get; set; }
        [ForeignKey("IdNguoiXoa")]
        public ApplicationUser GetNguoiXoa { get; set; }
    }
}
