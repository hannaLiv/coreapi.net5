﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class ApplicationRole : IdentityRole<int>
    {
        public int LoaiRole { get; set; } = 1;
        public int ParentId { get; set; } = 0;
        public int IsFullQuyen { get; set; } = 0;
    }
}
