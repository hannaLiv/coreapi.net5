﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class ThongBao : Base
    {
        public bool IsHeThongGui { get; set; } = false;
        public int? IdNguoiGui { get; set; }

        public bool IsGuiAll { get; set; } = false;
        
    }
}
