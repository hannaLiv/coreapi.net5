﻿using CoreMigroup.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class SanPham : Base
    {
        public int DichVuId { get; set; } = 0;
        public int NhaCungCapId { get; set; } = 0;
        public double GiaBan { get; set; } = 0;
        public double GiaKhuyenMai { get; set; } = 0;
        public int DonViTinh { get; set; } = (int)DonViTinhEnum.KhongXacDinh;
        public int SoLuong { get; set; } = 0;
        public double GiaNhap { get; set; }

        [ForeignKey("DichVuId")]
        public DichVu GetDichVuId { get; set; }

        [ForeignKey("NhaCungCapId")]
        public NhaCungCap GetNhaCungCapId { get; set; }

    }
}
