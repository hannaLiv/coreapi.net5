﻿using CoreMigroup.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class KhachHang : Base
    {
        public DateTime? NgaySinh { get; set; } = null;
        public string NguonKhachHang { get; set; } = "";
        public string DanhMucKhachHang { get; set; } = "";
        public int GioiTinh { get; set; } = (int)LoaiGioiTinhEnum.KhongXacDinh;
        public string Email { get; set; } = "";
        public string SoDienThoai { get; set; } = "";
        public string DiaChi { get; set; } = "";
        public string SoCMND { get; set; } = "";
        public int NhanVienPhuTrach { get; set; } = 0;

    }
}
