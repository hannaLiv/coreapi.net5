﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class ChiTietThongBao : Base
    {
        public int IdThongBao { get; set; } = 0;
        public int IdNguoiNhan { get; set; } = 0;
        public string NoiDungThongBao { get; set; } = "";
        public bool IsDaXem { get; set; } = false;

        [ForeignKey("IdThongBao")]
        public virtual ThongBao GetThongBao { get; set; }
    }
}
