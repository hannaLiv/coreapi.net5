﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string TenDayDu { get; set; } = "";
        public int LoaiUser { get; set; } = 1;
        public bool IsActive { get; set; } = true;
        public int ParentId { get; set; } = 0;
        public string ImagePath { get; set; } = "";
        public string IdCard { get; set; } = "";
        public string Address { get; set; } = "";
        public byte Gender { get; set; } = 0;
        public string NotificationTokens { get; set; } = "";
    }
}
