﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class DataDoanhNghiep : Base
    {
        public string MaSoThue { get; set; } = "";
        public string TenCongTy { get; set; } = "";
        public string TenTiengAnh { get; set; } = "";
        public string SoDienThoai { get; set; } = "";
        public string Email { get; set; } = "";
        public string DiaChiTruSo { get; set; } = "";
        public string Fax { get; set; } = "";
        public string Website { get; set; } = "";
        public string TenNguoiDaiDien { get; set; } = "";
        public string TenVietTat { get; set; } = "";
        public string LoaiHinhPhapLy { get; set; } = "";
        public int TinhTrangHoatDong { get; set; } = 0;
        public DateTime NgayThanhLap { get; set; } = DateTime.MinValue;
        public string TinhThanh { get; set; } = "";
        public string MaNganhChinh { get; set; } = "";
        public string TenNganhChinh { get; set; } = "";
        public string NhaMangDienThoai { get; set; } = "";
    }
}
