﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class OrderNhap : Base
    {
        public int SanPhamId { get; set; } = 0;
        public int SoLuong { get; set; } = 0;
        public double GiaNhap { get; set; } = 0;

        [ForeignKey("SanPhamId")]
        public SanPham GetSanPhamId { get; set; }
    }
}
