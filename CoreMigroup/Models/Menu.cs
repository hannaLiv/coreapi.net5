﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Models
{
    public class Menu : Base
    {
        public string DuongDan { get; set; } = "";
        public int? ParentId { get; set; } = null;
        public string TenQuyenTuongUng { get; set; } = "";

        [ForeignKey("ParentId")]
        public virtual Menu GetMenu { get; set; }
    }
}
