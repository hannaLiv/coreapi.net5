﻿using CoreAPI.JWTHelper;
using CoreMigroup.Helper;
using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CoreMigroup.ViewModel.UsersControllerViewModel;

namespace CoreMigroup.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IAuthenticateService _authen;
        private IUserRepository _userRepository;
        public UsersController(IAuthenticateService authen, IUserRepository userRepository)
        {
            _authen = authen;
            _userRepository = userRepository;
        }
        [HttpPost]
        [Route("LoginAsync")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginAsyncViewModel request)
        {
            if(request != null)
            {
                if (!string.IsNullOrEmpty(request.userName))
                {
                    if (!string.IsNullOrEmpty(request.passWord))
                    {
                        var rq = new AuthenticateRequest() { Username = request.userName, Password = request.passWord };
                        var res = await _authen.AuthenticateAsync(rq);
                        if (res != null)
                        {
                            return Ok(res);
                        }
                    }
                }
            }
            
            return BadRequest();    
        }

        [HttpPost]
        [Route("SaveNotificationToken")]
        public async Task<IActionResult> SaveNotificationTokens([FromBody] SaveNotificationTokensViewModel request)
        {
            var response = new ResponseData<int>();
            if(request == null)
            {
                response.IsSuccess = false;
                response.Message = "Có lỗi xảy ra, vui lòng thử lại!";
                return BadRequest(response);
            }
            else if(request.user_id <= 0)
            {
                response.IsSuccess = false;
                response.Message = "User không có ID, vui lòng thử lại";
                return BadRequest(response);
            }
            else if (string.IsNullOrEmpty(request.token))
            {
                response.IsSuccess = false;
                response.Message = "Không có token, vui lòng thử lại";
                return BadRequest(response);
            }
            else
            {
                var res = await _userRepository.SaveNotificationTokens(request.token, request.user_id);
                if (res)
                {
                    response.IsSuccess = res;
                    response.Message = "Thành công!";
                    return Ok(response);
                }
            }
            return BadRequest();
        }
    }
}
