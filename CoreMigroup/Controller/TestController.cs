﻿using CoreAPI.JWTHelper;
using CoreMigroup.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TestController : ControllerBase
    {

        [HttpGet("Index")]
        public IActionResult Index()
        {
            dynamic res = new ExpandoObject();
            res.Test = "abcd";
            res.Quang = "fghj";
            return Ok(res);
        }
        [HttpPost("Index1")]
        public IActionResult Index1(Index1ViewModel request)
        {
            if (request != null)
                if (!string.IsNullOrEmpty(request.stringTest))
                    return Ok(request.stringTest);
            return BadRequest();
        }
    }
}
