﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Repository.NhaCungCapRepository;
using MemoryCaching;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhaCungCapsController : ControllerBase
    {
        private INhaCungCapRepository _nhaCungCapRepository;
        private IDichVuRepository _dichVuRepository;
        private ICacheController _cacheController;
        public NhaCungCapsController(INhaCungCapRepository nhaCungCapRepository, ICacheController cacheController, IDichVuRepository dichVuRepository)
        {
            _nhaCungCapRepository = nhaCungCapRepository;
            _cacheController = cacheController;
            _dichVuRepository = dichVuRepository;
        }

        [HttpGet("GetAllNhaCungCap")]
        public IActionResult GetAllNhaCungCap()
        {
            var response = new ResponseData<object>();
            var key_cache = string.Format("GetAllNhaCungCap");
            var cached = _cacheController.Get(key_cache);
            if(!string.IsNullOrEmpty(cached))
            {
                
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NhaCungCap>>(cached);
                if(result != null)
                {
                    var query = from r in result select new { id = r.Id, ten = r.Ten };
                    response.IsSuccess = true;
                    response.Message = "Thành công";
                    var ttt = query.ToList();
                    response.Datas = query.ToList<object>();
                    return Ok(response);
                }
                
                
            }
            else
            {
                var result = _nhaCungCapRepository.GetAll();
                //Ghi vao cache
                if(result != null)
                {
                    _cacheController.Create(key_cache, Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    var query = from r in result select new { id = r.Id, ten = r.Ten };
                    response.IsSuccess = true;
                    response.Message = "Thành công";
                    response.Datas = query.ToList<object>();
                    return Ok(response);
                }
            }
            return BadRequest();

        }
        [HttpGet("GetAllDichVu")]
        public IActionResult GetAllDichVu()
        {
            var response = new ResponseData<object>();
            var key_cache = string.Format("GetAllDichVu");
            var cached = _cacheController.Get(key_cache);
            if (!string.IsNullOrEmpty(cached))
            {

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DichVu>>(cached);
                if (result != null)
                {
                    var query = from r in result select new { id = r.Id, ten = r.Ten };
                    response.IsSuccess = true;
                    response.Message = "Thành công";
                    var ttt = query.ToList();
                    response.Datas = query.ToList<object>();
                    return Ok(response);
                }


            }
            else
            {
                var result = _dichVuRepository.GetAll();
                //Ghi vao cache
                if (result != null)
                {
                    _cacheController.Create(key_cache, Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    var query = from r in result select new { id = r.Id, ten = r.Ten };
                    response.IsSuccess = true;
                    response.Message = "Thành công";
                    response.Datas = query.ToList<object>();
                    return Ok(response);
                }
            }
            return BadRequest();

        }

        [HttpPost("GetNhaCungCapByDichVuId")]
        public async Task<IActionResult> GetNhaCungCapByDichVuIdAsync([FromBody] p_GetNhaCungCapByDichVuId request)
        {
            var response = new ResponseData<NhaCungCap>();
            if(request != null)
            {
                var result = await _nhaCungCapRepository.GetListNhaCungCapByDichVuIdAsync(request);
                response.IsSuccess = true;
                response.Datas = result.nhaCungCaps;
                response.TotalRecord = result.total;
                response.Message = "Thành công";
                return Ok(response);
            }
            response.IsSuccess = false;
            response.Message = "Không tìm thấy id dịch vụ";
            response.Datas = null;
            response.TotalRecord = 0;
            return BadRequest(response);
        }



    }
}
