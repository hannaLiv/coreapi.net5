﻿using CoreMigroup.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreMigroup.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
        public DbSet<ThongBao> ThongBaos { get; set; }
        public DbSet<ChiTietThongBao> ChiTietThongBaos { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<FileUpload> FileUploads { get; set; }
        public DbSet<DataDoanhNghiep> DataDoanhNghieps { get; set; }
        public DbSet<KhachHang> KhachHangs { get; set; }
        public DbSet<DichVu> DichVus { get; set; }
        public DbSet<NhaCungCap> NhaCungCaps { get; set; }
        public DbSet<SanPham> SanPhams { get; set; }
        public DbSet<OrderNhap> OrderNhaps { get; set; }
    }
}
