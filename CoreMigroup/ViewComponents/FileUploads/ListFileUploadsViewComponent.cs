﻿using CoreMigroup.Enum;
using CoreMigroup.Repository.FileUploadRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.FileUploads
{
    public class ListFileUploadsViewComponent : ViewComponent
    {
        private IFileUploadRepository _fileUploadRepository;
        public ListFileUploadsViewComponent(IFileUploadRepository fileUploadRepository)
        {
            _fileUploadRepository = fileUploadRepository;
        }
        public IViewComponentResult Invoke(param_GetFileUpload request)
        {

            var model = _fileUploadRepository.GetAll().Include(r => r.GetFolder).Include(r => r.GetNguoiTao)
                .Where(r => r.IsDaXoa != true).Where(r => r.Loai == (int)LoaiFileEnum.Excel);
            if (request.FolderIds.Count > 0)
                model = model.Where(r => request.FolderIds.Contains(r.FolderId));
            if (request.NgayUploadTu != null)
                model = model.Where(r => r.NgayTao >= request.NgayUploadTu.Value);
            if (request.NgayUploadDen != null)
                model = model.Where(r => r.NgayTao <= request.NgayUploadDen.Value);
            if (!string.IsNullOrEmpty(request.Keyword))
                model = model.Where(r => r.Ten.Contains(request.Keyword) || r.Ma.Contains(request.Keyword) || r.GetFolder.Ma.Contains(request.Keyword) || r.GetFolder.Ten.Contains(request.Keyword));
            var total = model.Count();
            if (request.Index.Value > 0 && request.Size.Value > 0)
                model = model.OrderByDescending(r => r.NgayTao).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);
            
            
            ViewBag.Total = total;
            ViewBag.Index = request.Index.Value;
            ViewBag.Size = request.Size.Value;

            return View("~/Views/FileUploads/Components/ListFileUploads.cshtml", model.ToList());
        }
    }
}
