﻿using CoreMigroup.Models;
using CoreMigroup.Repository.FileUploadRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.FileUploads
{
    public class AddorUpdateFileUploadViewComponent : ViewComponent
    {
        private IFileUploadRepository _fileUploadRepository;
        public AddorUpdateFileUploadViewComponent(IFileUploadRepository fileUploadRepository)
        {
            _fileUploadRepository = fileUploadRepository;
        }
        public IViewComponentResult Invoke(int id)
        {
            var model = new FileUpload();
            if (id > 0)
                model = _fileUploadRepository.GetById(id);
            return View("~/Views/FileUploads/Components/SaveFileUpload.cshtml", model);
        }
    }
}
