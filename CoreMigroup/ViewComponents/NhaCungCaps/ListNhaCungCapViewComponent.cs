﻿using CoreMigroup.Models;
using CoreMigroup.Repository.NhaCungCapRepository;
using CoreMigroup.Repository.SanPhamRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.NhaCungCaps
{
    public class ListNhaCungCapViewComponent : ViewComponent
    {
        private INhaCungCapRepository _nhaCungCapRepository;
        private ISanPhamRepository _sanPhamRepository;

        public ListNhaCungCapViewComponent(INhaCungCapRepository nhaCungCapRepository, ISanPhamRepository sanPhamRepository)
        {
            _nhaCungCapRepository = nhaCungCapRepository;
            _sanPhamRepository = sanPhamRepository;
        }

        public IViewComponentResult Invoke(p_GetNhaCungCapByDichVuId request)
        {

            //var model = _nhaCungCapRepository.GetListNhaCungCapByDichVuId(request);
            //if (!string.IsNullOrEmpty(request.Keyword))
            //{
            //    model = model.Where(x => x.Ten.Contains(request.Keyword) || x.Email.Contains(request.Keyword)|| x.QuocGia.Contains(request.Keyword) || x.Ma.Contains(request.Keyword) || x.TenTaiKhoan.Contains(request.Keyword));
            //}

            var rs = new List<NhaCungCap>();
            rs = _nhaCungCapRepository.GetListNhaCungCapByDichVuIdAsync(request);
            //var idNCC = _sanPhamRepository.GetAll().Where(x => x.DichVuId.Equals(request.dichVuId)).Select(y => y.NhaCungCapId);
         
            //foreach (var item in idNCC)
            //{
            //    var ncc = new NhaCungCap();
            //    var l = _nhaCungCapRepository.GetAll().FirstOrDefault(x => x.Id.Equals(item));
            //    ncc.Id = l.Id;
            //    ncc.Ten = l.Ten;
            //    ncc.Ma = l.Ma;
            //    ncc.SoDienThoai = l.SoDienThoai;
            //    ncc.QuocGia = l.QuocGia;
            //    ncc.Email = l.Email;
            //    ncc.DiaChi = l.DiaChi;
            //    ncc.TenTaiKhoan = l.TenTaiKhoan;
            //    ncc.SoTaiKhoan = l.SoTaiKhoan;
            //    ncc.NganHang = l.NganHang;
            //    rs.Add(ncc);
                
            //}
            //rs = rs.GroupBy(x => x.Id).Select(z => z.First()).ToList();
            //if(!String.IsNullOrEmpty(request.Keyword))
            //{
            //    rs = rs.Where(x => x.Ten.Contains(request.Keyword) || x.Email.Contains(request.Keyword) || x.QuocGia.Contains(request.Keyword) || x.Ma.Contains(request.Keyword) || x.TenTaiKhoan.Contains(request.Keyword)).ToList();
            //}
            var total = rs.Count();

            ViewBag.Total = total;
                ViewBag.Index = request.Index.Value;
                ViewBag.Size = request.Size.Value;
            if (request.Index.Value > 0 && request.Size.Value > 0)
                rs = rs.OrderByDescending(r => r.NgayTao).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value).ToList();
            return View("~/Views/NhaCungCaps/Components/GetListNCC.cshtml", rs.ToList());
            

        }
    }
}
