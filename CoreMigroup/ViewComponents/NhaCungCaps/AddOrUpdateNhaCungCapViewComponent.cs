﻿using CoreMigroup.Models;
using CoreMigroup.Repository.NhaCungCapRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.NhaCungCaps
{
    public class AddOrUpdateNhaCungCapViewComponent : ViewComponent
    {
        private INhaCungCapRepository _nhaCCRepo;
        public AddOrUpdateNhaCungCapViewComponent(INhaCungCapRepository nhaCCRepo)
        {
            _nhaCCRepo = nhaCCRepo;
        }
        public IViewComponentResult Invoke(int id)
        {
            var model = new NhaCungCap();
            if (id > 0)
                model = _nhaCCRepo.GetById(id);
            return View("~/Views/NhaCungCaps/Components/AddOrUpdateNCC.cshtml", model);
        }
    }
}
