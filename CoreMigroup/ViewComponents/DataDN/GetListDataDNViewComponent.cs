﻿using CoreMigroup.Repository.DataDoanhNghiepRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.DataDN
{
    public class GetListDataDNViewComponent : ViewComponent 
    {
        private IDataDoanhNghiepRepository _dataDNRepo;
        public GetListDataDNViewComponent (IDataDoanhNghiepRepository dataDN)
        {
            _dataDNRepo = dataDN;
        }
        public IViewComponentResult Invoke(param_GetDataDN request)
        {

            var t = 0;
            var res = _dataDNRepo.GetListDataDN(request, out t);

            ViewBag.Total = t;
            ViewBag.Index = request.Index.Value;
            ViewBag.Size = request.Size.Value;

            return View("~/Views/DataDoanhNghiep/Components/GetListDataDN.cshtml", res);
        }
    }
}
