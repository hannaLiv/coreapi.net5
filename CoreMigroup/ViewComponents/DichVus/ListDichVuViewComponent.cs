﻿using CoreMigroup.Repository.DichVuRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.DichVus
{
    public class ListDichVuViewComponent : ViewComponent
    {
        private IDichVuRepository _dichVuRpositoey;

        public ListDichVuViewComponent(IDichVuRepository dichVuRepository)
        {
            _dichVuRpositoey = dichVuRepository;
        }

        public IViewComponentResult Invoke(param_GetListDV request)
        {
            var model = _dichVuRpositoey.GetAll().Include(r => r.GetNguoiTao).Where(x => x.IsDaXoa != true);
            if (!String.IsNullOrEmpty(request.Keyword))
            {
                model = model.Where(x => x.Ten.Contains(request.Keyword));
            }
            var total = model.Count();
            if (request.Index.Value > 0 && request.Size.Value > 0)
                model = model.OrderByDescending(r => r.NgayTao).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);
            ViewBag.Total = total;
            ViewBag.Index = request.Index.Value;
            ViewBag.Size = request.Size.Value;
            return View("~/Views/DichVus/Components/GetListDichVu.cshtml", model.ToList());
        }
    }
}
