﻿using CoreMigroup.Models;
using CoreMigroup.Repository.DichVuRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.DichVus
{
    public class AddOrUpdateDichVuViewComponent : ViewComponent
    {
        private IDichVuRepository _dichVuRepositoery;
        public AddOrUpdateDichVuViewComponent(IDichVuRepository dichVuRepositoery)
        {
            _dichVuRepositoery = dichVuRepositoery;
        }
        public IViewComponentResult Invoke(int id)
        {
            var model = new DichVu();
            if (id > 0)
                model = _dichVuRepositoery.GetById(id);
            return View("~/Views/DichVus/Components/AddOrUpdateDichVu.cshtml", model);
        }
    }
}
