﻿using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.Users
{
    public class GetListUsersViewComponent : ViewComponent
    {
        private IUserRepository _userdRepository;
        public GetListUsersViewComponent(IUserRepository userdRepository)
        {
            _userdRepository = userdRepository;
        }
        public IViewComponentResult Invoke(param_FilterUser request)
        {

            var t = 0;
            var res = _userdRepository.GetList(request, out t);

            ViewBag.Total = t;
            ViewBag.Index = request.Index.Value;
            ViewBag.Size = request.Size.Value;

            return View("~/Views/Users/Components/GetListUser.cshtml", res);
        }
    }
}
