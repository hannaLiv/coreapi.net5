﻿using CoreMigroup.Data;
using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.Users
{
    public class GetByIdViewComponent : ViewComponent
    {
        private IUserRepository _userdRepository;
        private ApplicationDbContext _db;
        public GetByIdViewComponent(IUserRepository userdRepository, ApplicationDbContext db)
        {
            _userdRepository = userdRepository;
            _db = db;
        }
        public IViewComponentResult Invoke(int userId)
        {
            //var allRows = _db.Users.ToList();
            ViewBag.UserId = userId;
            var id = _db.Users.SingleOrDefault(x => x.Id == userId);
            
            return View("~/Views/Users/Components/GetById.cshtml", id);
        }
    }
}
