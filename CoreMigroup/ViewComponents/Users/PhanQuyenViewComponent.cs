﻿using CoreMigroup.Data;
using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.Users
{
    public class PhanQuyenViewComponent : ViewComponent
    {
        private IUserRepository _userdRepository;
        private ApplicationDbContext _db;
        public PhanQuyenViewComponent(IUserRepository userdRepository, ApplicationDbContext db)
        {
            _userdRepository = userdRepository;
            _db = db;
        }
        public IViewComponentResult Invoke(int userId)
        {
            var allRows = _db.Roles.ToList();
            ViewBag.UserId = userId;

            return View("~/Views/Users/Components/PhanQuyen.cshtml", allRows);
        }
    }
}
