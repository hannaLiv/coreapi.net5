﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents
{
    public class FileInFolderViewComponent :ViewComponent
    {
        public IViewComponentResult Invoke(int folderId)
        {
            ViewBag.FolderId = folderId;
            return View("~/Views/Shared/Components/QuanLyFile/FileInFolder.cshtml");
        }
    }
}
