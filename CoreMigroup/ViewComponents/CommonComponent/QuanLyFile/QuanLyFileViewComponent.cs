﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents
{
    public class QuanLyFileViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke() {
            ViewBag.Test = 1;
            return View("~/Views/Shared/Components/QuanLyFile/QuanLyFile.cshtml");
        }
    }
}
