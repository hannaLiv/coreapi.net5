﻿using CoreMigroup.Helper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.CommonComponent.ThongBao
{
    public class ListThongBaoViewComponent : ViewComponent
    {
        private readonly IUserHelper _userHelper;
        public ListThongBaoViewComponent(IUserHelper userHelper)
        {
            _userHelper = userHelper;
        }
        public IViewComponentResult Invoke(int? index, int? size)
        {
            index = index ?? 1;
            size = size ?? 10;
            ViewBag.UserId = _userHelper.GetCurrentUserAsync().Result.Id;
            ViewBag.Index = index;
            ViewBag.Size = size;
            return View("~/Views/Shared/Components/ThongBao/ListThongBao.cshtml");
        }
    }
}
