﻿using CoreMigroup.Helper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.CommonComponent.ThongBao
{
    public class UserThongBaoViewComponent : ViewComponent
    {
        private IUserHelper _userHelper;
        public UserThongBaoViewComponent(IUserHelper userHelper)
        {
            _userHelper = userHelper;
        }
        public IViewComponentResult Invoke()
        {
            var currentUser = _userHelper.GetCurrentUserAsync().Result;
            ViewBag.Test = 1;
            return View("~/Views/Shared/Components/ThongBao/UserThongBao.cshtml", currentUser);
        }
    }
}
