﻿using CoreMigroup.Repository.KhachHangRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.ViewComponents.KhachHangs
{
    public class ListKhachHangViewComponent : ViewComponent
    {
        private IKhachHangRepository _khachHangRepository;

        public ListKhachHangViewComponent(IKhachHangRepository khachHangRepository)
        {
            _khachHangRepository = khachHangRepository;
        }
        public IViewComponentResult Invoke(param_GetlistKhachHang request)
        {
            var model = _khachHangRepository.GetAll().Include(r => r.GetNguoiTao).Where(x => x.IsDaXoa != true);
            if(request.DanhMuc.Count > 0)
            {
                model = model.Where(x => request.DanhMuc.Contains(x.DanhMucKhachHang));
            }
            if(request.Nguon.Count > 0)
            {
                model = model.Where(x => request.Nguon.Contains(x.NguonKhachHang));
            }
            if(request.NgayUploadTu != null)
            {
                model = model.Where(x => x.NgayTao >= request.NgayUploadTu);
            }
            if (request.NgayUploadDen != null)
            {
                model = model.Where(x => x.NgayTao <= request.NgayUploadTu);
            }
            if (!string.IsNullOrEmpty(request.Keyword))
            {
                model = model.Where(x => x.Ten.Contains(request.Keyword) || x.Email.Contains(request.Keyword) || x.SoCMND.Contains(request.Keyword) || x.SoDienThoai.Contains(request.Keyword)|| x.DiaChi.Contains(request.Keyword));
            }
            var total = model.Count();
            if (request.Index.Value > 0 && request.Size.Value > 0)
                model = model.OrderByDescending(r => r.NgayTao).Skip((request.Index.Value - 1) * request.Size.Value).Take(request.Size.Value);
            ViewBag.Total = total;
            ViewBag.Index = request.Index.Value;
            ViewBag.Size = request.Size.Value;

            return View("~/Views/KhachHangs/Components/GetListKhachHang.cshtml",model.ToList());
        }
    }
}
