﻿using CoreMigroup.Repository.KhachHangRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreMigroup.Models;

namespace CoreMigroup.ViewComponents.KhachHangs
{
    public class AddOrUpdateKhachHangViewComponent : ViewComponent
    {
        private IKhachHangRepository _khachHangRepository;
        public AddOrUpdateKhachHangViewComponent(IKhachHangRepository khachHangRepository)
        {
            _khachHangRepository = khachHangRepository;
        }
        public IViewComponentResult Invoke(int id)
        {
            var model = new KhachHang();
            if (id > 0)
                model = _khachHangRepository.GetById(id);
            return View("~/Views/KhachHangs/Components/AddOrUpdateKhachHang.cshtml", model);
        }
    }
}
