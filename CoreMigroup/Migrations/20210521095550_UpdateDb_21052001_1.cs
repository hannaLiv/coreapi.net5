﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreMigroup.Migrations
{
    public partial class UpdateDb_21052001_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "GiaNhap",
                table: "SanPhams",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "SoLuong",
                table: "SanPhams",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DiaChi",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NganHang",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuocGia",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SoDienThoai",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SoTaiKhoan",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenTaiKhoan",
                table: "NhaCungCaps",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GiaNhap",
                table: "SanPhams");

            migrationBuilder.DropColumn(
                name: "SoLuong",
                table: "SanPhams");

            migrationBuilder.DropColumn(
                name: "DiaChi",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "NganHang",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "QuocGia",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "SoDienThoai",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "SoTaiKhoan",
                table: "NhaCungCaps");

            migrationBuilder.DropColumn(
                name: "TenTaiKhoan",
                table: "NhaCungCaps");
        }
    }
}
