﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreMigroup.Migrations
{
    public partial class UpdateDb29042021_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DataDoanhNghieps",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaSoThue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenCongTy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenTiengAnh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SoDienThoai = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DiaChiTruSo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fax = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Website = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenNguoiDaiDien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenVietTat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoaiHinhPhapLy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TinhTrangHoatDong = table.Column<int>(type: "int", nullable: false),
                    NgayThanhLap = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TinhThanh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaNganhChinh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenNganhChinh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NhaMangDienThoai = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    MetaData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThai = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdNguoiTao = table.Column<int>(type: "int", nullable: true),
                    NgayTao = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNguoiCapNhat = table.Column<int>(type: "int", nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDaXoa = table.Column<bool>(type: "bit", nullable: true),
                    IdNguoiXoa = table.Column<int>(type: "int", nullable: true),
                    NgayXoa = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataDoanhNghieps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataDoanhNghieps_AspNetUsers_IdNguoiCapNhat",
                        column: x => x.IdNguoiCapNhat,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataDoanhNghieps_AspNetUsers_IdNguoiTao",
                        column: x => x.IdNguoiTao,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataDoanhNghieps_AspNetUsers_IdNguoiXoa",
                        column: x => x.IdNguoiXoa,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataDoanhNghieps_IdNguoiCapNhat",
                table: "DataDoanhNghieps",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_DataDoanhNghieps_IdNguoiTao",
                table: "DataDoanhNghieps",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_DataDoanhNghieps_IdNguoiXoa",
                table: "DataDoanhNghieps",
                column: "IdNguoiXoa");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataDoanhNghieps");
        }
    }
}
