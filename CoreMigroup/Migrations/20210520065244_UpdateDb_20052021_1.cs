﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreMigroup.Migrations
{
    public partial class UpdateDb_20052021_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DichVus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<int>(type: "int", nullable: false),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    MetaData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThai = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdNguoiTao = table.Column<int>(type: "int", nullable: true),
                    NgayTao = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNguoiCapNhat = table.Column<int>(type: "int", nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDaXoa = table.Column<bool>(type: "bit", nullable: true),
                    IdNguoiXoa = table.Column<int>(type: "int", nullable: true),
                    NgayXoa = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DichVus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DichVus_AspNetUsers_IdNguoiCapNhat",
                        column: x => x.IdNguoiCapNhat,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DichVus_AspNetUsers_IdNguoiTao",
                        column: x => x.IdNguoiTao,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DichVus_AspNetUsers_IdNguoiXoa",
                        column: x => x.IdNguoiXoa,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NhaCungCaps",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<int>(type: "int", nullable: false),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    MetaData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThai = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdNguoiTao = table.Column<int>(type: "int", nullable: true),
                    NgayTao = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNguoiCapNhat = table.Column<int>(type: "int", nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDaXoa = table.Column<bool>(type: "bit", nullable: true),
                    IdNguoiXoa = table.Column<int>(type: "int", nullable: true),
                    NgayXoa = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhaCungCaps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NhaCungCaps_AspNetUsers_IdNguoiCapNhat",
                        column: x => x.IdNguoiCapNhat,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NhaCungCaps_AspNetUsers_IdNguoiTao",
                        column: x => x.IdNguoiTao,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NhaCungCaps_AspNetUsers_IdNguoiXoa",
                        column: x => x.IdNguoiXoa,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SanPhams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DichVuId = table.Column<int>(type: "int", nullable: false),
                    NhaCungCapId = table.Column<int>(type: "int", nullable: false),
                    GiaBan = table.Column<double>(type: "float", nullable: false),
                    GiaKhuyenMai = table.Column<double>(type: "float", nullable: false),
                    DonViTinh = table.Column<int>(type: "int", nullable: false),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    MetaData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThai = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdNguoiTao = table.Column<int>(type: "int", nullable: true),
                    NgayTao = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNguoiCapNhat = table.Column<int>(type: "int", nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDaXoa = table.Column<bool>(type: "bit", nullable: true),
                    IdNguoiXoa = table.Column<int>(type: "int", nullable: true),
                    NgayXoa = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SanPhams_AspNetUsers_IdNguoiCapNhat",
                        column: x => x.IdNguoiCapNhat,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SanPhams_AspNetUsers_IdNguoiTao",
                        column: x => x.IdNguoiTao,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SanPhams_AspNetUsers_IdNguoiXoa",
                        column: x => x.IdNguoiXoa,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SanPhams_DichVus_DichVuId",
                        column: x => x.DichVuId,
                        principalTable: "DichVus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPhams_NhaCungCaps_NhaCungCapId",
                        column: x => x.NhaCungCapId,
                        principalTable: "NhaCungCaps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderNhaps",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SanPhamId = table.Column<int>(type: "int", nullable: false),
                    SoLuong = table.Column<int>(type: "int", nullable: false),
                    GiaNhap = table.Column<double>(type: "float", nullable: false),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    MetaData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThai = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdNguoiTao = table.Column<int>(type: "int", nullable: true),
                    NgayTao = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNguoiCapNhat = table.Column<int>(type: "int", nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDaXoa = table.Column<bool>(type: "bit", nullable: true),
                    IdNguoiXoa = table.Column<int>(type: "int", nullable: true),
                    NgayXoa = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderNhaps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderNhaps_AspNetUsers_IdNguoiCapNhat",
                        column: x => x.IdNguoiCapNhat,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderNhaps_AspNetUsers_IdNguoiTao",
                        column: x => x.IdNguoiTao,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderNhaps_AspNetUsers_IdNguoiXoa",
                        column: x => x.IdNguoiXoa,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderNhaps_SanPhams_SanPhamId",
                        column: x => x.SanPhamId,
                        principalTable: "SanPhams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DichVus_IdNguoiCapNhat",
                table: "DichVus",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_DichVus_IdNguoiTao",
                table: "DichVus",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_DichVus_IdNguoiXoa",
                table: "DichVus",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCaps_IdNguoiCapNhat",
                table: "NhaCungCaps",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCaps_IdNguoiTao",
                table: "NhaCungCaps",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCaps_IdNguoiXoa",
                table: "NhaCungCaps",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNhaps_IdNguoiCapNhat",
                table: "OrderNhaps",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNhaps_IdNguoiTao",
                table: "OrderNhaps",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNhaps_IdNguoiXoa",
                table: "OrderNhaps",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNhaps_SanPhamId",
                table: "OrderNhaps",
                column: "SanPhamId");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_DichVuId",
                table: "SanPhams",
                column: "DichVuId");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_IdNguoiCapNhat",
                table: "SanPhams",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_IdNguoiTao",
                table: "SanPhams",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_IdNguoiXoa",
                table: "SanPhams",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_NhaCungCapId",
                table: "SanPhams",
                column: "NhaCungCapId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderNhaps");

            migrationBuilder.DropTable(
                name: "SanPhams");

            migrationBuilder.DropTable(
                name: "DichVus");

            migrationBuilder.DropTable(
                name: "NhaCungCaps");
        }
    }
}
