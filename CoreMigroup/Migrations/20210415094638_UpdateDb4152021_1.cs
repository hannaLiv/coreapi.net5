﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreMigroup.Migrations
{
    public partial class UpdateDb4152021_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TenQuyenTuongUng",
                table: "Menus",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IsFullQuyen",
                table: "AspNetRoles",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenQuyenTuongUng",
                table: "Menus");

            migrationBuilder.DropColumn(
                name: "IsFullQuyen",
                table: "AspNetRoles");
        }
    }
}
