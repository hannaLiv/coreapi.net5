﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreMigroup.Migrations
{
    public partial class UpdateDb20210413_4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "ThongBaos",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "ThongBaos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "Menus",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "Menus",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "Folders",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "Folders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "FileUploads",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "FileUploads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "ChiTietThongBaos",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "ChiTietThongBaos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_ThongBaos_IdNguoiCapNhat",
                table: "ThongBaos",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_ThongBaos_IdNguoiTao",
                table: "ThongBaos",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_ThongBaos_IdNguoiXoa",
                table: "ThongBaos",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_IdNguoiCapNhat",
                table: "Menus",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_IdNguoiTao",
                table: "Menus",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_IdNguoiXoa",
                table: "Menus",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_Folders_IdNguoiCapNhat",
                table: "Folders",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_Folders_IdNguoiTao",
                table: "Folders",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_Folders_IdNguoiXoa",
                table: "Folders",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_FileUploads_IdNguoiCapNhat",
                table: "FileUploads",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_FileUploads_IdNguoiTao",
                table: "FileUploads",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_FileUploads_IdNguoiXoa",
                table: "FileUploads",
                column: "IdNguoiXoa");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietThongBaos_IdNguoiCapNhat",
                table: "ChiTietThongBaos",
                column: "IdNguoiCapNhat");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietThongBaos_IdNguoiTao",
                table: "ChiTietThongBaos",
                column: "IdNguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietThongBaos_IdNguoiXoa",
                table: "ChiTietThongBaos",
                column: "IdNguoiXoa");

            migrationBuilder.AddForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiCapNhat",
                table: "ChiTietThongBaos",
                column: "IdNguoiCapNhat",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiTao",
                table: "ChiTietThongBaos",
                column: "IdNguoiTao",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiXoa",
                table: "ChiTietThongBaos",
                column: "IdNguoiXoa",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiCapNhat",
                table: "FileUploads",
                column: "IdNguoiCapNhat",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiTao",
                table: "FileUploads",
                column: "IdNguoiTao",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiXoa",
                table: "FileUploads",
                column: "IdNguoiXoa",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiCapNhat",
                table: "Folders",
                column: "IdNguoiCapNhat",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiTao",
                table: "Folders",
                column: "IdNguoiTao",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiXoa",
                table: "Folders",
                column: "IdNguoiXoa",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiCapNhat",
                table: "Menus",
                column: "IdNguoiCapNhat",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiTao",
                table: "Menus",
                column: "IdNguoiTao",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiXoa",
                table: "Menus",
                column: "IdNguoiXoa",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiCapNhat",
                table: "ThongBaos",
                column: "IdNguoiCapNhat",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiTao",
                table: "ThongBaos",
                column: "IdNguoiTao",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiXoa",
                table: "ThongBaos",
                column: "IdNguoiXoa",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiCapNhat",
                table: "ChiTietThongBaos");

            migrationBuilder.DropForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiTao",
                table: "ChiTietThongBaos");

            migrationBuilder.DropForeignKey(
                name: "FK_ChiTietThongBaos_AspNetUsers_IdNguoiXoa",
                table: "ChiTietThongBaos");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiCapNhat",
                table: "FileUploads");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiTao",
                table: "FileUploads");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUploads_AspNetUsers_IdNguoiXoa",
                table: "FileUploads");

            migrationBuilder.DropForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiCapNhat",
                table: "Folders");

            migrationBuilder.DropForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiTao",
                table: "Folders");

            migrationBuilder.DropForeignKey(
                name: "FK_Folders_AspNetUsers_IdNguoiXoa",
                table: "Folders");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiCapNhat",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiTao",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_AspNetUsers_IdNguoiXoa",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiCapNhat",
                table: "ThongBaos");

            migrationBuilder.DropForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiTao",
                table: "ThongBaos");

            migrationBuilder.DropForeignKey(
                name: "FK_ThongBaos_AspNetUsers_IdNguoiXoa",
                table: "ThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_ThongBaos_IdNguoiCapNhat",
                table: "ThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_ThongBaos_IdNguoiTao",
                table: "ThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_ThongBaos_IdNguoiXoa",
                table: "ThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_Menus_IdNguoiCapNhat",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_IdNguoiTao",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_IdNguoiXoa",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Folders_IdNguoiCapNhat",
                table: "Folders");

            migrationBuilder.DropIndex(
                name: "IX_Folders_IdNguoiTao",
                table: "Folders");

            migrationBuilder.DropIndex(
                name: "IX_Folders_IdNguoiXoa",
                table: "Folders");

            migrationBuilder.DropIndex(
                name: "IX_FileUploads_IdNguoiCapNhat",
                table: "FileUploads");

            migrationBuilder.DropIndex(
                name: "IX_FileUploads_IdNguoiTao",
                table: "FileUploads");

            migrationBuilder.DropIndex(
                name: "IX_FileUploads_IdNguoiXoa",
                table: "FileUploads");

            migrationBuilder.DropIndex(
                name: "IX_ChiTietThongBaos_IdNguoiCapNhat",
                table: "ChiTietThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_ChiTietThongBaos_IdNguoiTao",
                table: "ChiTietThongBaos");

            migrationBuilder.DropIndex(
                name: "IX_ChiTietThongBaos_IdNguoiXoa",
                table: "ChiTietThongBaos");

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "ThongBaos",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "ThongBaos",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "Menus",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "Menus",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "Folders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "Folders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "FileUploads",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "FileUploads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayTao",
                table: "ChiTietThongBaos",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdNguoiTao",
                table: "ChiTietThongBaos",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
