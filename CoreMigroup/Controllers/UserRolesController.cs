﻿using CoreMigroup.Helper;
using CoreMigroup.Repository.UserRoleRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class UserRolesController : BaseController
    {
        private IUserRoleRepository _userRoleRepository;
        public UserRolesController(IUserRoleRepository userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }
        public IActionResult ModalPhanQuyen(int userId)
        {
            return ViewComponent("PhanQuyen", new { userId = userId });
        }

        [HttpPost]
        public IActionResult SaveQuyenByUserId(param_SaveQuyen request)
        {
            var responseData = new ResponseData<int>();
            if(request != null)
            {
                var checker = _userRoleRepository.SaveQuyen(request);
                responseData.IsSuccess = checker;
                return Ok(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Có lỗi xảy ra, vui lòng thử lại!";
            return BadRequest(responseData);
        }
    }
}
