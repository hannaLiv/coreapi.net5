﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.FolderRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class FoldersController : BaseController
    {
        private IFolderRepository _folderRepo;
        private readonly IWebHostEnvironment _env;
        private readonly IUserHelper _user;
        private readonly IConfiguration _config;
        public FoldersController(IFolderRepository folderRepo, IWebHostEnvironment env, IUserHelper user, IConfiguration config)
        {
            _folderRepo = folderRepo;
            _env = env;
            _user = user;
            _config = config;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFolder(param_InsertFolder request)
        {
            
            ResponseData<Folder> response = new ResponseData<Folder>();
            if(request != null)
            {
                var ten = request.ten;
                var parentId = request.parentId;
                if (!string.IsNullOrEmpty(ten))
                {
                    var checker = _folderRepo.GetAll().Where(r => r.Ten.Equals(ten) && r.ParentId == parentId);
                    if (checker.Count() <= 0)
                    {
                        var f = new Folder();
                        f.Ten = ten;
                        f.ParentId = parentId == null ? null : parentId;
                        //Tao thu muc cho no
                        var webRootPath = _env.WebRootPath;
                        var user = _user.GetCurrentUserAsync().Result.Id;
                        var path = string.Format("UserID_{0}\\{1}", user, ten);
                        var save_path = path;
                        path = webRootPath + "\\Upload\\" + path;
                        var uploadBaseFolder = _config.GetValue<string>("FileSetting:UploadBaseFolder");
                        var path_save_database = "\\" +uploadBaseFolder + "\\" + save_path;
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        f.DuongDan = path_save_database;
                        var r = await _folderRepo.Insert(f);
                        
                        response.IsSuccess = true;
                        response.Datas.Add(r);
                        response.TotalRecord = 1;
                        
                        return Ok(response);
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.ErrorMessage = "Đã có tên folder, vui lòng thử tên khác";
                        return BadRequest(response);
                    }

                }
            }
            
            response.IsSuccess = false;
            response.ErrorMessage = "Tên không được để trống";
            return BadRequest(response);
        }
    }
}
