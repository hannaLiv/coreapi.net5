﻿using CoreMigroup.Repository.DataDoanhNghiepRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class DataDoanhNghiepController : Controller
    {
        public IActionResult ListDataDoanhNghiep()
        {
            return View();
        }
        [HttpPost]
        public IActionResult GetListDataDN(param_GetDataDN request)
        {

            return ViewComponent("GetListDataDN", new { request = request });
        }
    }
}
