﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.KhachHangRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class KhachHangsController : BaseController
    {
        private IUserHelper _userHelper;
        private IKhachHangRepository _khachHangRepository;
        public KhachHangsController(IUserHelper userHelper, IKhachHangRepository khachHangRepository)
        {
            _userHelper = userHelper;
            _khachHangRepository = khachHangRepository;
        }
        public IActionResult ListKhachHang()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ViewAddOrEdit(int id)
        {
            return ViewComponent("AddOrUpdateKhachHang", new { id = id });
        }

        [HttpPost]

        public async Task<IActionResult> AddOrEdit_KhachHang(param_KhachHang request)
        {
            var responseData = new ResponseData<KhachHang>();

            if (request != null)
            {
                //var d = request.NhanVienPTId;
                var  result = await _khachHangRepository.AddOrEdit_KhachHang(request);
                responseData.IsSuccess = true;
                responseData.Datas.Add(result);
                return Ok(responseData);
            }

            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        }
        [HttpPost]
        public IActionResult FilterKhachHang(param_GetlistKhachHang request)
        {
            return ViewComponent("ListKhachHang", new { request = request });
        }
    }
}
