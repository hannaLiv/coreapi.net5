﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.SanPhamRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class SanPhamsController : BaseController
    {
        private IUserHelper _userHelper;
        private ISanPhamRepository _sanPhamRepository;
        public SanPhamsController(IUserHelper userHelper, ISanPhamRepository sanPhamRepository)
        {
            _userHelper = userHelper;
            _sanPhamRepository = sanPhamRepository; ;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddOrUpdate_SP(param_AddOrUpdate_SP request)
        {
            var responseData = new ResponseData<SanPham>();
            if (request != null)
            {
                var result = await _sanPhamRepository.AddOrEdit_SP(request);
                responseData.IsSuccess = true;
                responseData.Datas.Add(result);
                return Ok(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSPById(int id)
        {
            var responseData = new ResponseData<SanPham>();
            if (id != 0)
            {
                var result = await _sanPhamRepository.DeleteById(id);
                responseData.IsSuccess = true;
                responseData.Datas.Add(result);
                return Ok(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        }
    }
}
