﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Repository.NhaCungCapRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class DichVusController : BaseController
    {
        private IUserHelper _userHelper;
        private IDichVuRepository _dichVuRepository;
        private INhaCungCapRepository _nhaCungCapRepository;
        public DichVusController(IUserHelper userHelper, IDichVuRepository dichVuRepository, INhaCungCapRepository nhaCungCapRepository)
        {
            _userHelper = userHelper;
            _dichVuRepository = dichVuRepository;
            _nhaCungCapRepository = nhaCungCapRepository;
        }
        public IActionResult ListDichVu()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddOrUpdate(int id)
        {
            return ViewComponent("AddOrUpdateDichVu", new { id = id });
        }
        [HttpPost]

        public async Task<IActionResult> AddOrEdit_DichVu(param_DichVu request)
        {
            var responseData = new ResponseData<DichVu>();
            if(request != null)
            {
                var result = await _dichVuRepository.AddOrEdit_DichVu(request);
                responseData.IsSuccess = true;
                responseData.Datas.Add(result);
                return Ok(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        } 
        [HttpPost]

        public IActionResult FilterDichVu(param_GetListDV request)
        {
            var check = request.Keyword;
            return ViewComponent("ListDichVu", new { request = request });
        }
        [HttpPost]

        public async Task<IActionResult> Delete_Dichvu(int id)
        {
            var responseData = new ResponseData<DichVu>();
            if(id != 0)
            {
                var res = await _dichVuRepository.Delete_DichVu(id);
                responseData.IsSuccess = true;
                responseData.Datas.Add(res);
                return Ok(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        }


        
    }
}
