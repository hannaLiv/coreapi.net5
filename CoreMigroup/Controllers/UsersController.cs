﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private IUserRepository _repo;
        public UsersController(IUserRepository repo)
        {
            _repo = repo;
        }
        [Authorize(Roles = "Admin,HienThiMenuUser")]
        public IActionResult ListUser()
        {
            return View();
        }
        [HttpPost]
        public IActionResult GetListUser(param_FilterUser request)
        {
            return ViewComponent("GetListUsers", new { request = request});
        }
        public IActionResult ModalGetById(int userId)
        {
            return ViewComponent("GetById", new { userId = userId });
        }

    }
}
