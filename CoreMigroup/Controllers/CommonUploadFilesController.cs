﻿using CoreMigroup.Enum;
using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.FileUploadRepository;
using CoreMigroup.Repository.FolderRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class CommonUploadFilesController : BaseController
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        private readonly IFolderRepository _folderRepo;
        private readonly IFileUploadRepository _fileUploadRepo;
        public CommonUploadFilesController(IWebHostEnvironment env, IConfiguration config, IFolderRepository folderRepo, IFileUploadRepository fileUploadRepo)
        {
            _env = env;
            _config = config;
            _folderRepo = folderRepo;
            _fileUploadRepo = fileUploadRepo;
        }
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFiles(List<IFormFile> request, int folderId = 44)
        {
            var response = new ResponseData<FileUpload>();
            if (request.Count >= 0)
            {
                var allowTypeList = new List<string>();
                var allowTypes = _config.GetValue<string>("FileSetting:AllowType");
                if (!string.IsNullOrEmpty(allowTypes))
                {
                    allowTypeList = allowTypes.Split(';').ToList();
                }
                var maxSize = _config.GetValue<long>("FileSetting:MaxSize");
                var webRootPath = _env.WebRootPath;
                //var webRootPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //var rp = _env.WebRootFileProvider.GetDirectoryContents(webRootPath);
                //var mapPath = Path.GetPathRoot(webRootPath);
                var lsFiles = Request.Form.Files;

                foreach (var item in lsFiles)
                {
                    var fileExt = Path.GetExtension(item.FileName);
                    var matchedExt = allowTypeList.Where(r => r.Trim() == fileExt).FirstOrDefault();
                    if (matchedExt != null && item.Length < maxSize)
                    {
                        var now = DateTime.Now;
                        var tick = now.Ticks;
                        var fileName = tick + "_" + item.FileName;
                        var fileInfo = new FileUpload();
                        fileInfo.FolderId = folderId;
                        //get duong dan cua folder
                        var folderSeleced = _folderRepo.GetById(folderId);
                        var folderDir = "";
                        if (folderSeleced != null)
                        {
                            folderDir = folderSeleced.DuongDan;
                        }
                        var duongDanFile = folderSeleced.DuongDan + "\\" + fileName;
                        var duongDanLuu = webRootPath + "\\" + duongDanFile;
                        fileInfo.DuongDan = duongDanFile;
                        fileInfo.KichThuoc = (int)item.Length;
                        var loaiFile = 0;
                        var avatar = "";
                        switch (fileExt)
                        {
                            case ".doc":
                                loaiFile = (int)LoaiFileEnum.DOC;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".docx":
                                loaiFile = (int)LoaiFileEnum.DOC;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".xls":
                                loaiFile = (int)LoaiFileEnum.Excel;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".xlsx":
                                loaiFile = (int)LoaiFileEnum.Excel;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".jpg":
                                loaiFile = (int)LoaiFileEnum.JPG;
                                break;
                            case ".png":
                                loaiFile = (int)LoaiFileEnum.PNG;
                                break;
                            case ".svg":
                                loaiFile = (int)LoaiFileEnum.SVG;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".pdf":
                                loaiFile = (int)LoaiFileEnum.PDF;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".json":
                                loaiFile = (int)LoaiFileEnum.Json;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".xml":
                                loaiFile = (int)LoaiFileEnum.Xml;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            case ".html":
                                loaiFile = (int)LoaiFileEnum.Html;
                                avatar = "\\images\\thumbnail\\excel_thumb.png";
                                break;
                            default:
                                loaiFile = (int)LoaiFileEnum.KhongXacDinh;
                                break;
                        }
                        fileInfo.DuoiFile = fileExt;
                        fileInfo.Loai = loaiFile;
                        fileInfo.Avatar = avatar;
                        fileInfo.Ten = item.FileName;
                        var t = new FileUpload();


                        using (var stream = new FileStream(duongDanLuu, FileMode.Create))
                        {
                            //luu file vao database,
                            
                            //luu file vao o cung ma toi chon
                            item.CopyTo(stream);
                            //luu anh thumb neu la kieu anh
                            
                        }
                        if (fileExt.Equals(".jpg") || fileExt.Equals(".png"))
                        {
                            //Bat dau ve cai anh nay cho no nho di
                            

                            var duongDanThumb = webRootPath + "\\" + folderDir + "\\thumbnail\\" + fileName;
                            var folderThumb = webRootPath + "\\" + folderDir + "\\thumbnail";
                            var duongDanThumb_saveAvatar = folderDir + "\\thumbnail\\" + fileName;
                            if (!Directory.Exists(folderThumb))
                            {
                                Directory.CreateDirectory(folderThumb);
                            }

                            var thumb_avatar_file = ResizeImageHelper.Image_resize(duongDanLuu, duongDanThumb);
                            fileInfo.Avatar = duongDanThumb_saveAvatar;
                        }
                        //save vao database
                        t = await _fileUploadRepo.Insert(fileInfo);
                        response.Datas.Add(t);

                    }


                }
                response.IsSuccess = true;
                response.TotalRecord = response.Datas.Count();
                return Ok(response);


            }
            response.IsSuccess = false;
            response.ErrorMessage = "Bạn chưa chọn file nào";
            response.TotalRecord = 0;
            return BadRequest(response);
        }
    }
}
