﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.DichVuRepository;
using CoreMigroup.Repository.NhaCungCapRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    [Route("NhaCungCaps")]
    public class NhaCungCapsController : BaseController
    {
        private IUserHelper _userHelper;
        private IDichVuRepository _dichVuRepository;
        private INhaCungCapRepository _nhaCungCapRepository;
        public NhaCungCapsController(IUserHelper userHelper, IDichVuRepository dichVuRepository, INhaCungCapRepository nhaCungCapRepository)
        {
            _userHelper = userHelper;
            _dichVuRepository = dichVuRepository;
            _nhaCungCapRepository = nhaCungCapRepository;
        }
        [Route("ListTheoDichVu/{id}")]
        public IActionResult Index(int id)
        {
            //Console.WriteLine(id);
            var requset = new p_GetNhaCungCapByDichVuId();
            requset.dichVuId = id;
            //var lsNhaCungCap = _nhaCungCapRepository.GetListNhaCungCapByDichVuId(requset);
            return View(id);
        }
        [Route("ViewAddOrUpdate")]
        [HttpPost]
        public IActionResult AddOrUpdate(p_GetNhaCungCapById request)
        {
            if(request != null)
            {
                return ViewComponent("AddOrUpdateNhaCungCap", new { id = request.id });
            }
            return null;
            
        }

        //[Route("AddOrUpdate")]
        //[HttpPost]
        //public async Task<IActionResult> AddOrUpdate(param_AddOrUpdateNCC request)
        //{
        //    var responseData = new ResponseData<NhaCungCap>();
        //    var id = request.id;
        //    if(request != null)
        //    {
        //        var result = await _nhaCungCapRepository.AddOrEdit_NCC(request);
        //        responseData.IsSuccess = true;
        //        responseData.Datas.Add(result);
        //        return Ok(result);
        //    }
        //    responseData.IsSuccess = false;
        //    responseData.ErrorMessage = "Request không hợp lệ";

        //    return BadRequest(responseData);
        //}
        [Route("AddOrUpdate")]
        [HttpPost]
        public async Task<IActionResult> AddOrUpdate(param_SaveNhaCungCap request)
        {
            var responseData = new ResponseData<NhaCungCap>();
            if (request != null)
            {
                var result = await _nhaCungCapRepository.SaveNhaCungCap(request);
                responseData.IsSuccess = true;
                responseData.Datas.Add(result);
                return Ok(result);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";

            return BadRequest(responseData);
        }
        [Route("FilterNCC")]
        [HttpPost]
        public IActionResult FilterNCC(p_GetNhaCungCapByDichVuId request)
        {
            int id = request.dichVuId;
            return ViewComponent("ListNhaCungCap", new { request = request });
        }
    }
}
