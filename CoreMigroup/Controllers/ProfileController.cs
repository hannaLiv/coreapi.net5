﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.UserRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class ProfileController : Controller
    {
        private IUserRepository _userRepository;
        public ProfileController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAvatar(param_ChangeAvatar request)
        {
            var responseData = new ResponseData<string>();
            if(request != null)
            {
                if (!string.IsNullOrEmpty(request.url_Image))
                {
                    var result = await _userRepository.ChangeAvatarAsync(request.url_Image);
                    responseData.IsSuccess = true;
                    responseData.Datas.Add(result);
                    return Ok(responseData);
                }
                responseData.IsSuccess = false;
                responseData.ErrorMessage = "Avatar không được để trống";
                
                return BadRequest(responseData);
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Có lỗi xảy ra, vui lòng thử lại";
            return BadRequest(responseData);

        }
    }
}
