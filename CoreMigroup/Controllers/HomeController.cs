﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserHelper _user;

        public HomeController(ILogger<HomeController> logger, IUserHelper user)
        {
            _logger = logger;
            _user = user;
        }


        public IActionResult FirstLoad()
        {
            if (_user.CheckUserLogin())
            {
                return View("~/Views/Home/Index.cshtml");
            }
            return Redirect("/Identity/Account/Login");
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Test()
        {
            return View();
        }
    }
}
