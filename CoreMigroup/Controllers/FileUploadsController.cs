﻿using CoreMigroup.Helper;
using CoreMigroup.Models;
using CoreMigroup.Repository.FileUploadRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CoreMigroup.Controllers
{
    [Authorize]
    public class FileUploadsController : BaseController
    {
        private IFileUploadRepository _fileUploadRepository;
        private ICrawlingExcelHelper _crawling;
        public FileUploadsController(IFileUploadRepository fileUploadRepository, ICrawlingExcelHelper crawling)
        {
            _fileUploadRepository = fileUploadRepository;
            _crawling = crawling;
        }
        [Authorize(Roles = "Admin,HienThiMenuFile,XemDanhSachFile")]
        
        public IActionResult GetList()
        {
            return View();
        }
        //[Authorize(Roles = "Admin,HienThiMenuFile")]
        [HttpPost]

        public IActionResult LoadFilesInFolder(int folderId)
        {
            return ViewComponent("FileInFolder", new { folderId = folderId });
        }
        [HttpPost]
        public IActionResult Find(int[] folderId, DateTime startDate, DateTime endDate, string keyword)
        {


            List<IQueryable<FileUpload>> lstFile = new List<IQueryable<FileUpload>>();
            var allFile = _fileUploadRepository.GetAll();
            allFile = allFile.Where(x =>
            (folderId.Length > 0 && folderId.Select(y => y).Contains(x.FolderId))
            || (keyword != null && x.Ten.ToLower().Trim().Contains(keyword.ToLower().Trim()))
            || (x.NgayTao >= startDate && x.NgayTao <= endDate));

            allFile = allFile.Include(r => r.GetFolder).Include(r => r.GetNguoiTao).Where(r => r.IsDaXoa != true);


            return View("~/Views/FileUploads/Components/ListFileUploads.cshtml", allFile.ToList());
        }
        //[Authorize(Roles = "Admin,HienThiMenuFile")]
        [HttpPost]
        public IActionResult FilterFiles(param_GetFileUpload request)
        {
            return ViewComponent("ListFileUploads", new { request = request });
        }
        [HttpPost]
        public IActionResult ViewAddEditFileUpload(int id)
        {
            return ViewComponent("AddorUpdateFileUpload", new { id = id });
        }

        [HttpPost]
        public async Task<IActionResult> AddOrUpdateFileUpload_DangKyDoanhNghiep([FromForm] param_SaveFileUpload request)
        {
            var responseData = new ResponseData<FileUpload>();
            if(request != null)
            {
                if(request.lsFiles.Count <= 0 && request.id == 0)
                {
                    responseData.IsSuccess = false;
                    responseData.ErrorMessage = "Không có file được chọn";
                    return BadRequest(responseData);
                }
                else if (string.IsNullOrEmpty(request.tenFile))
                {
                    responseData.IsSuccess = false;
                    responseData.ErrorMessage = "Tên file không được để trống";
                    return BadRequest(responseData);
                }
                else if (request.danhMucFile == 0 && string.IsNullOrEmpty(request.danhMucTen))
                {
                    responseData.IsSuccess = false;
                    responseData.ErrorMessage = "Mời chọn danh mục file";
                    return BadRequest(responseData);
                }
                else
                {
                    var r = await _fileUploadRepository.AddOrUpdateFileUpload_ExcelDoanhNghiep(request);
                    responseData.IsSuccess = true;
                    responseData.Datas.Add(r);
                    return Ok(responseData);
                }
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Request không hợp lệ";
            return BadRequest(responseData);
        }

        [HttpPost]
        public async Task<IActionResult> DongBoDuLieu_DoanhNghiep(int id)
        {
            var responseData = new ResponseData<int>();
            if(id > 0)
            {
                var totalRecord = await _crawling.ImportExcelDoanhNghiepAsync(id);
                if(totalRecord > 0)
                {
                    responseData.IsSuccess = true;
                    responseData.TotalRecord = totalRecord;
                    return Ok(responseData);
                }
            }
            responseData.IsSuccess = false;
            responseData.ErrorMessage = "Có lỗi xảy ra! Vui lòng thử lại";
            return BadRequest(responseData);
        }
    }
}

    