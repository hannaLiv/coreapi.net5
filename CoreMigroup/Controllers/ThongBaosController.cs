﻿using CoreMigroup.Helper;
using CoreMigroup.Repository.ThongBaoRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.Controllers
{
    public class ThongBaosController : BaseController
    {
        private IThongBaoRepository _thongBaoRepository;
        public ThongBaosController(IThongBaoRepository thongBaoRepository)
        {
            _thongBaoRepository = thongBaoRepository;
        }
        public IActionResult Index()
        {

            return View();
        }
        public IActionResult Test()
        {
            return Ok(_thongBaoRepository.Test());
        }
        [HttpPost]
        public IActionResult GetThongBaos(int? index, int? size)
        {
            return ViewComponent("ListThongBao", new { index = index, size = size });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateDaXemThongBao(param_UpdateThongBaoDaXem request)
        {
            var response = new ResponseData<int>();
            if (request != null)
            {
                try
                {
                    var r = await _thongBaoRepository.UpdateDaXemThongBao(request.lsId);
                    response.IsSuccess = r;
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                    return BadRequest(response);
                    throw;
                }
            }
            response.ErrorMessage = "Không có list Id";
            return BadRequest(response);
        }
    }
}
