﻿using CoreMigroup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.SignalRHub
{
    public class ThongBaoViewModel
    {
        public string user { get; set; }
        public string message { get; set; }
    }
}
