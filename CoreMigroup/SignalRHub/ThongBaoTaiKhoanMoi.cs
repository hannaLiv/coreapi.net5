﻿using CoreMigroup.Repository.ThongBaoRepository;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMigroup.SignalRHub
{
    public interface IThongBaoTaiKhoanMoi
    {
        Task ThongBaoNewUser(ThongBaoViewModel tb);
        Task ThongBaoNewUser_FromServer(param_CreateThongBao p);
    }
    public class ThongBaoTaiKhoanMoi : Hub, IThongBaoTaiKhoanMoi
    {
        private IHubContext<ThongBaoTaiKhoanMoi> _hub;
        public ThongBaoTaiKhoanMoi(IHubContext<ThongBaoTaiKhoanMoi> hub)
        {
            _hub = hub;
        }
        public async Task ThongBaoNewUser(ThongBaoViewModel tb)
        {
            //await Clients.All.SendAsync("ReceiveMessage", user, message);
            await Clients.All.SendAsync("ThongBaoNewUser_On", tb.user, tb.message);
        }



        public async Task ThongBaoNewUser_FromServer(param_CreateThongBao p)
        {
            if (p != null)
            {
                var lsNguoiNhan = p.IdsNguoiNhan;
                var ls = new List<string>();
                foreach (var item in lsNguoiNhan)
                    ls.Add(item.ToString());
                IReadOnlyList<string> l = ls;

                await _hub.Clients.Users(l).SendAsync("ThongBaoNewUser_FromServer_On", p.NoiDungThongBao);
            }
            //await _hub.Clients.All.SendAsync("ThongBaoNewUser_FromServer_On", string.Format("Tài khoản {0} mới đăng ký mới", user_name));
        }
    }
}
