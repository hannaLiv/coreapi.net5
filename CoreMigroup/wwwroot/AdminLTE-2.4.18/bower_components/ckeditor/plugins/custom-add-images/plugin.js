﻿CKEDITOR.plugins.add('custom-add-images', {
    icons: 'image',
    init: function (editor) {
        editor.addCommand('insertImages', {
            exec: function (editor) {
                R.UploadFileComponent.OpenModalQuanLyFile();
                $('#dinh-kem-file').off('click').on('click', function () {
                    var lsImage = [];
                    $('.file-choosen').each(function (e) {
                        var url_image = $(this).find('img').attr('src');
                        lsImage.push(url_image);
                        var name_ck = "";
                        R.UploadFileComponent.AddChoosenFileToCkeditor(name_ck, lsImage, function () {
                            var htm = "";
                            lsImage.forEach(function (element) {
                                htm += '<img src="' + element + '">';
                                htm += '<br>';
                            })
                            editor.insertHtml(htm);
                        })

                    })
                })
            }
        });
        //Plugin logic goes here.
        editor.ui.addButton('CustomInsertImages', {
            label: 'Insert Images',
            command: 'insertImages',
            toolbar: 'insert'
        });
    }
});