﻿
R.NhaCungCap = {
    Init: function () {
        R.NhaCungCap.RegisterEvent();
        R.NhaCungCap.LoadNhaCungCap();
    },

    RegisterEvent: function () {
        $('#btn-create-ncc').off('click').on('click', function () {
            R.NhaCungCap.ViewAddOrEdit(0);
        })
        $('#btn-add-san-pham').off('click').on('click', function () {
            console.log('add');
            var $temp = $('#template-add-san-pham');
            var $cloned = $temp.last().clone();
            $cloned.removeAttr('id');
            $cloned.css('display', 'unset');
            $cloned.addClass('cloned-san-pham');
            $('#binding-san-pham-ncc').append($cloned);
            R.NhaCungCap.RegisterEvent();
        })
        
        //$('#luu-khach-hang').off('click').on('click', function () {
        //    var $lsSanPham = $('.cloned-san-pham');
        //    $lsSanPham.each(function (element) {
        //        console.log($(this).find('.txt-gia-ban').data('giatri'));
        //    })
        //})
        $('#save-nha-cung-cap-form').off('submit').on('submit', function () {
            
            var id = $(this).data('id');
            //console.log(id);
            R.NhaCungCap.AddOrEdit_NhaCC(id);
            //R.NhaCungCap.AddOrUpdate_SP(3);
            return false;
        })
        $('.edit-ncc').off('click').on('click', function () {
            
            var id = $(this).data('id');
            R.NhaCungCap.ViewAddOrEdit(id);
        })
        $('#btn-tim-kiem-ncc').off('click').on('click', function () {
            R.NhaCungCap.LoadNhaCungCap();
        })
        $('.xoa-sp').off('click').on('click', function () {
           
            //if (R.Confirm()) {
            //    var id = $(this).attr("data-idSp");
            //    //alert(id);
            //    R.NhaCungCap.XoaSP(id);
            //    $(this).parents(".cloned-san-pham").remove();
            //}
            $(this).parents(".cloned-san-pham").remove();   
        })
        $('.xoa').off('click').on('click', function () {
            $(this).parents(".cloned-san-pham").remove();
        })
        R.RegisterEvent();
    },
    ViewAddOrEdit: function (id) {
        url = "/NhaCungCaps/ViewAddOrUpdate"
        params = {
            id: id
        }
        $.post(url, params, function (res) {
            $('#save-nha-cung-cap-modal').html('').html(res);
            $('#save-nha-cung-cap-modal').modal('show');
            R.NhaCungCap.RegisterEvent();
        })
    },
    AddOrEdit_NhaCC: function (id) {
        //console.log(id);
        var Id = id;
        var MaNCC = $('#txt-ma-ncc').val();
        var TenNCC = $('#txt-ten-ncc').val();
        var Sdt = $('#txt-sdt').val();
        var Email = $('#txt-email').val();
        var TenQuocGia = $('#txt-ten-quoc-gia').val();
        var DiaChi = $('#txt-dia-chi').val();
        var TenTK = $('#txt-ten-tai-khoan').val();
        var SoTK = $('#txt-stk').val();
        var TenNganHang = $('#txt-ten-ngan-hang').val();
        var url = window.location.pathname;
        var idDV = parseInt(url.substring(url.lastIndexOf('/') + 1));
        var nhaCungCap = {
            Id: Id,
            Ma: MaNCC,
            Ten: TenNCC,
            SoDienThoai: Sdt,
            Email: Email,
            QuocGia: TenQuocGia,
            DiaChi: DiaChi,
            TenTaiKhoan: TenTK,
            SoTaiKhoan: SoTK,
            NganHang: TenNganHang
        }
        //Tao ra danh sach san  pham
        var sanPhams = [];
        var $lsSanPham = $('.cloned-san-pham');
        $lsSanPham.each(function (element) {
            var sp = {
                Id: $(this).data('idsp'),
                DichVuId: idDV,
                NhaCungCapId: Id,
                GiaBan: parseFloat($(this).find('.txt-gia-ban').val()),
                GiaKhuyenMai: parseFloat($(this).find('.txt-gia-khuyen-mai').val()),
                GiaNhap: parseFloat($(this).find('.txt-gia-nhap').val()),
                DonViTinh: $(this).find('.txt-don-vi-tinh').val(),
                GhiChu: $(this).find('.txt-ghi-chu').val(),
                Ten: $(this).find('.txt-ten-san-pham').val()
            }
            sanPhams.push(sp);
        })
        var params = {
            nhaCungCap: nhaCungCap,
            sanPhams: sanPhams
        }
        
        console.log(params);
        var url = "/NhaCungCaps/AddOrUpdate"
        $.post(url, params, function (res) {
            $('#save-nha-cung-cap-modal').modal('hide');
            R.PopupNotify("Thêm mới nhà cung cấp thành công", "success");
            R.NhaCungCap.LoadNhaCungCap();
            R.NhaCungCap.RegisterEvent();
            return false;
            //alert(res.id);
            //R.NhaCungCap.AddOrUpdate_SP(res.id);
        })
    },
    AddOrUpdate_SP: function (id) {
        var url = window.location.pathname;
        var idDV = url.substring(url.lastIndexOf('/') + 1);
        var Ten = [];
        var GiaBan = [];
        var GiaKhuyenMai = [];
        var GiaNhap = [];
        var DonViTinh = [];
        var GhiChu = [];
        var Id = [];
        var idNCC = id;
        var $lsSanPham = $('.cloned-san-pham');
        $lsSanPham.each(function (element) {
            //console.log($(this).find('.txt-ten-san-pham').val());
            Ten.push($(this).find('.txt-ten-san-pham').val());
            GiaBan.push($(this).find('.txt-gia-ban').val());
            GiaKhuyenMai.push($(this).find('.txt-gia-khuyen-mai').val());
            GiaNhap.push($(this).find('.txt-gia-nhap').val());
            DonViTinh.push($(this).find('.txt-don-vi-tinh').val());
            GhiChu.push($(this).find('.txt-ghi-chu').val());
            if ($(this).find('input').attr("data-sp") == "") {
                alert('no');
                Id.push(0);
            } else {
                Id.push($(this).find('input').attr("data-sp"));
            }
            


        })
        
        var url = "/SanPhams/AddOrUpdate_SP";
        var params = {
            id: Id,
            IdNCC: idNCC,
            IdDV: idDV,
            Ten: Ten,
            GiaBan: GiaBan,
            GiaNhap: GiaNhap,
            GiaKhuyenMai: GiaKhuyenMai,
            DonViTinh: DonViTinh,
            GhiChu: GhiChu
        }
        $.post(url, params, function () {
            $('#save-nha-cung-cap-modal').modal('hide');
            R.PopupNotify("Thêm mới sản phẩm thành công", "success");
            R.NhaCungCap.LoadNhaCungCap();
            R.NhaCungCap.RegisterEvent();
        })
        
    },
    LoadNhaCungCap: function (index) {
        var index = index ?? 1;
        var size = 10;
        var url = window.location.pathname;
        var idDV = url.substring(url.lastIndexOf('/') + 1);
        var keyWord = $('#txb-keyword-khach-hang').val() ?? "";

        var params = {
            dichVuId: idDV,
            Index: index,
            Size: size,
            keyword: keyWord
        }
        var url = "/NhaCungCaps/FilterNCC"
        console.log(params);
        $.post(url, params, function (res) {
            //alert('oke');
            //console.log(res);
            $('.binding-data').html('').html(res);
            R.NhaCungCap.PhanTrangFile();
            R.NhaCungCap.RegisterEvent();
        })
    },
    PhanTrangFile: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        R.Pagination($binding_phan_trang, index, total, size, function () {

            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            R.NhaCungCap.LoadKhachHang(pageNum);
            R.NhaCungCap.RegisterEvent();
        })

    },
    XoaSP: function (id) {
        var Id = id;
        var url = "/SanPhams/DeleteSPById"
        var params = {
            id: Id
        }
        $.post(url, params, function (res) {
            R.PopupNotify("xóa sản phẩm thành công", "success");
            R.NhaCungCap.LoadNhaCungCap();
            R.NhaCungCap.RegisterEvent();
        })
    }
}
R.NhaCungCap.Init();