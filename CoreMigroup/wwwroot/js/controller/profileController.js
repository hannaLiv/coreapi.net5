﻿R.Profile = {
    Init: function () {
        R.Profile.RegisterEvent();
    },
    RegisterEvent: function () {
        $('#openFile').off('click').on('click', function () {
            console.log(1);
            R.UploadFileComponent.OpenModalQuanLyFile();
        })
        $('#dinh-kem-file').off('click').on('click', function () {
            var lsImage = [];
            $('.file-choosen').each(function (e) {
                var url_image = $(this).find('img').attr('src');
                lsImage.push(url_image);
            })
            if (lsImage.length > 0) {
                R.Profile.UpdateAvatar(lsImage[0]);
            }
            else {
                R.PopupNotify('Xin mời chọn ảnh', 'error');
            }
            
        })
    },
    UpdateAvatar: function (url_iamge) {
        var url = "/Profile/UpdateAvatar";
        var params = {
            url_Image: url_iamge
        }
        $.post(url, params, function (response) {
            console.log(response);
            R.PopupNotify('Update Avatar thành công', 'success');
            $('.img-circle').attr('src', response.datas[0]);
            R.UploadFileComponent.CloseModalQuanLyFile();
        })
    }
}

R.Profile.Init();