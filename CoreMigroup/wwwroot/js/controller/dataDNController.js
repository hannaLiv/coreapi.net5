﻿R.dataDN = {

    Init: function () {
        
        R.dataDN.RegisterEvent();
        R.dataDN.FindData();
        
    },
    RegisterEvent: function () {
    R.dataDN.PhanTrangData();
        $('#btn-tim-kiem-DN').off('click').on('click', function () {

            R.dataDN.FindData();

        })
    },
    PhanTrangData: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        
        R.Pagination($binding_phan_trang, index, total, size, function () {
            
            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            //console.log(pageNum);
            R.dataDN.FindData(pageNum);
            R.dataDN.RegisterEvent();

        })
    },
    FindData: function (index) {

        var keyword = $('#txb-keyword-dataDN').val();
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = index ?? 1;
        
        //var size = $div_phan_trang_container.data('size');
        var size = 10;
        var params = {
            Keyword: keyword,
            Index: index,
            Size: size
        }
        var url = '/DataDoanhNghiep/GetListDataDN'
        $.post(url, params, function (response) {
            $('.binding-data').html('').html(response);
            R.dataDN.PhanTrangData();
            //var $binding_phan_trang = $('.binding-phan-trang');
            //R.SetCurrentPage($binding_phan_trang, index);
            R.dataDN.RegisterEvent();
        })
    }

}
R.dataDN.Init();