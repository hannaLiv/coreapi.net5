﻿"use strict";
R.ThongBao = {
    Init: function () {
        //begin phan singalR
        R.ThongBao.Conn = new signalR.HubConnectionBuilder().withUrl("/thong-bao").build();
        Object.defineProperty(WebSocket, 'OPEN', { value: 1, });
        R.ThongBao.Conn.start().then(() => {
            console.log("Đã connect singalR")
        }).then(() => {
            console.log("Đã gửi thông báo");
        }).catch(function (err) {
            return console.error(err.toString());
        });
        R.ThongBao.NhanThongBao(R.ThongBao.Conn)
        //end phan singalR
        R.ThongBao.CurrentIndex = 1;
        R.ThongBao.CurrentSize = 5;
        R.ThongBao.BindingThongBaoUser();
        R.ThongBao.RegisterEvent();
    },
    RegisterEvent: function () {
        $('#test-signalR').off('click').on('click', function () {
            R.ThongBao.GuiThongBao(R.ThongBao.Conn, "ThongBaoNewUser", { user: "test", message: "test" });
        })
        $('#binding_list_thong_bao').off('scroll').on("scroll", function () {
            var scrollHeight = $(this).height();
            var scrollPosition = $(this).height() + $(this).scrollTop();
            console.log(scrollHeight, scrollPosition);
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                R.ThongBao.CurrentIndex++;
                R.ThongBao.BindingThongBaoUser();
                R.ThongBao.UpdateThongBaoDaXem();
            }
        });

    },
    GuiThongBao: function (conn, ten_ham_bat_thong_bao, obj_gui_di) {
        conn.invoke(ten_ham_bat_thong_bao, obj_gui_di).catch(function (err) {
            return console.error(err.toString());
        });

    },
    NhanThongBao: function (conn) {
        conn.on("ThongBaoNewUser_On", function (user, message) {
            R.ThongBao.AddSoThongBaoMoi();
        })
        //ThongBaoNewUser_FromServer_On
        conn.on("ThongBaoNewUser_FromServer_On", function (message) {
            R.ThongBao.AddSoThongBaoMoi();
        })
    },
    BindingThongBaoUser: function () {
        var url = "/ThongBaos/GetThongBaos"
        var params = {
            index: R.ThongBao.CurrentIndex,
            size: R.ThongBao.CurrentSize
        }
        $.post(url, params, function (response) {
            $('#binding_list_thong_bao').append(response);
            R.ThongBao.RegisterEvent()
        });
    },
    AddSoThongBaoMoi: function () {
        var current = parseInt($('#so-thong-bao-moi').text());
        console.log(current);
        current = current + 1;
        $('#so-thong-bao-moi').text('').text(current);
        R.ThongBao.RegisterEvent();
    },
    UpdateThongBaoDaXem: function () {
        var lsId = [];
        $('#binding_list_thong_bao').find('.un-seen').each(function (element) {
            lsId.push($(this).data('id'));
        })
        var url = "/ThongBaos/UpdateDaXemThongBao";
        var params = {
            lsId: lsId
        }
        $.post(url, params, function (response) {
            $('#binding_list_thong_bao').find('.un-seen').each(function (element) {
                $(this).removeClass('un-seen');
                $(this).css('background-color', 'unset');
            })
            R.ThongBao.RegisterEvent();
        })

    }
}
R.ThongBao.Init();