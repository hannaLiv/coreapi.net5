﻿R.UploadFileComponent = {
    Init: function () {
        R.UploadFileComponent.RegisterEvent();
    },
    RegisterEvent: function () {
        $('.open-upload-files-component').off('click').on('click', function () {
            R.UploadFileComponent.OpenModalQuanLyFile();
        })
        $('#add-folder').off('click').on('click', function () {
            $('#input-name-folder').css('display', '');
        })
        $('#input-name-folder').unbind('keypress').bind('keypress', function(e){
            if (e.keyCode == 13) {
                var ten = $(this).val();
                var parentId = null;
                var parameters = {
                    ten: ten,
                    parentId: parentId
                }
                R.UploadFileComponent.InsertFolder(parameters);
            }
        })
        $('#button-upload-files').off('click').on('click', function () {
            document.getElementById("input-upload-files").click();
        })
        $('#input-upload-files').off('change').on('change', function () {
            var files = $(this).prop('files');
            R.UploadFileComponent.UploadFiles(files);
            //alert(countFiles);
        })
        $('.folder-name').off('click').on('click', function () {
            $('.folder-name').each(function (e) {
                $(this).closest('li').removeClass('folder-active');
            })
            $(this).closest('li').addClass('folder-active');
            
            //binding-list-file
            R.UploadFileComponent.LoadFiles();
            
        })
        $('.select-file-in-folder').off('click').on('click', function () {
            $(this).parent().addClass('file-choosen');
        })
        
    },
    OpenModalQuanLyFile: function () {
        $('#quan-ly-files-modal').modal('show');
        R.UploadFileComponent.RegisterEvent();
    },
    InsertFolder: function (parameters) {
        var url = "/Folders/CreateFolder"
        $.ajax({
            url: url,
            data: parameters,
            method: 'POST',
            success: function (response) {
                if (response.isSuccess) {
                    alert('Tạo folder thành công');
                    console.log(response);
                    var cl = $('#folder-mau').last().clone();
                    cl.removeAttr('id');
                    cl.css('display', '');
                    cl.data('id', response.datas[0].id);
                    cl.find('.folder-name').text(parameters.ten);
                    $('#folder-mau').parent().prepend(cl);
                    $('#input-name-folder').val('');
                    $('#input-name-folder').css('display', 'none');

                    R.UploadFileComponent.RegisterEvent();
                }
            },
            error: function (response) {
                if (!response.isSuccess) {
                    alert(response.errorMessage);
                    R.UploadFileComponent.RegisterEvent();
                }
            }

        })
        //$.post(url, parameters, function (response) {
            
        //})
    },
    UploadFiles: function (files) {
        //console.log(files);
        var filesInput = Object.values(files);
        
        var folderId = 0;
        $('.folder-name').each(function (e) {
            var $li = $(this).closest('li');
            if ($li.hasClass('folder-active')) {
                folderId = $li.data('id');
            }
        })
        var dt = new FormData();
        filesInput.forEach(function (e) {
            dt.append('files',e);
        })
        dt.append('folderId', folderId);
        var url = "/CommonUploadFiles/UploadFiles";
        $.ajax({
            method: 'POST',
            url: url,
            contentType: false,
            processData: false,
            data: dt,
            success: function (response) {
                R.UploadFileComponent.LoadFiles();
                R.UploadFileComponent.RegisterEvent();

            },
            error: function (response) {
                R.UploadFileComponent.RegisterEvent();
            }
        })
        //files.forEach(function (e) {
        //    dt.append('file', e);
        //})
        //console.log(dt);

    },
    AddChoosenFileToCkeditor(ckeditor_name, lsImage, callback) {

        console.log(lsImage);
        $('#quan-ly-files-modal').modal('hide');
        if (typeof callback === 'function') {
            callback();
        }
        
    },
    LoadFiles: function () {
        var folderId = 0;
        $('.folder-name').each(function (e) {
            var $li = $(this).closest('li');
            if ($li.hasClass('folder-active')) {
                folderId = $li.data('id');
            }
        })
        var url = "/FileUploads/LoadFilesInFolder";
        var params = {
            folderId: folderId
        }
        $.post(url, params, function (response) {
            console.log(response);
            $('#binding-list-file').html('').html(response);
            R.UploadFileComponent.RegisterEvent();
        })
    },
    CloseModalQuanLyFile: function () {
        $('#quan-ly-files-modal').modal('hide');
        R.UploadFileComponent.RegisterEvent();
    },
}
R.UploadFileComponent.Init();