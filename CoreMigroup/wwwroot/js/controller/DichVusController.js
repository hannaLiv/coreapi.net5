﻿R.DichVu = {
    Init: function () {
        R.DichVu.RegisterEvent();
        R.DichVu.LoadDichVu();
    },
    RegisterEvent: function () {
        $('#btn-create-dv').off('click').on('click', function () {
            R.DichVu.ViewAddOrEdit(0);
        })

        $('#save-dich-vu-form').off('submit').on('submit', function () {
            var id = $(this).data('id');
            R.DichVu.AddOrEdit_DichVu(id);
            return false;
        })

        $('#btn-tim-kiem-dv').off('click').on('click', function () {
            
            R.DichVu.LoadDichVu();
        })

        $('.edit-dich-vu').off('click').on('click', function () {
            var id = $(this).data('id');
            R.DichVu.ViewAddOrEdit(id);
        })

        $('.delete-dich-vu').off('click').on('click', function () {
            if (R.Confirm()) {
                var id = $(this).data('id');
                R.DichVu.Delete_DichVu(id);
            }
        })
    },
    ViewAddOrEdit: function (id) {
        url = "/DichVus/AddOrUpdate"
        params = {
            id: id
        }
        $.post(url, params, function (res) {
            $('#save-dich-vu-modal').html('').html(res);
            $('#save-dich-vu-modal').modal('show');
            R.DichVu.RegisterEvent();
        })
    },

    AddOrEdit_DichVu: function (id) {
        var TenDv = $('#txt-ten-dv').val();
        var Id = id;
        var params = {
            Id: Id,
            Ten: TenDv
        }
        var url = "/DichVus/AddOrEdit_DichVu"
        $.post(url, params, function (res) {
            $('#save-dich-vu-modal').modal('hide');
            R.PopupNotify("Thêm mới dịch vụ thành công", "success");
            R.DichVu.LoadDichVu();
            R.DichVu.RegisterEvent();
        });
    },
    Delete_DichVu: function (id) {
        var Id = id;
        var params = {
            Id : id
        }
        var url = "/DichVus/Delete_Dichvu";

        $.post(url, params, function (res) {
            R.PopupNotify("Xóa dịch vụ thành công", "success");
            R.DichVu.LoadDichVu();
            R.DichVu.RegisterEvent();
        })
    },
    LoadDichVu: function (index) {
        var index = index ?? 1;
        var size = 10;

        var keyWord = $('#txb-keyword-dich-vu').val();
        var params = {
            Index: index,
            Size: size,
            KeyWord: keyWord
        }
        var url = "/DichVus/FilterDichVu";
        $.post(url, params, function (res) {
            $('.binding-data').html('').html(res);
            R.DichVu.PhanTrang();
            R.DichVu.RegisterEvent();
        })
    },
    PhanTrang: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        R.Pagination($binding_phan_trang, index, total, size, function () {

            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            R.DichVu.LoadDichVu(pageNum);
            R.DichVu.RegisterEvent();
        })

    }
}
R.DichVu.Init();