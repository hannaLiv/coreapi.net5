﻿R.Users = {
    Init: function () {
        R.Users.RegisterEvent();
        R.Users.Filter();
        //R.Users.PhanTrang();
    },
    RegisterEvent: function () {
        $('.open-modal-phan-quyen').off('click').on('click', function () {
            var userId = $(this).data('userid');
            R.Users.OpenPhanQuyenModal(userId);
            
        })
        $('.detail').off('click').on('click', function () {
            var userId = $(this).data('userid');
            R.Users.OpenGetByIdModal(userId);

        })
        
        $('.cbx-quyen-all').off('change').on('change', function () {

            if ($(this).is(':checked')) {
                $(this).closest('.modal').find('input[type=checkbox]').prop('disabled', true);
                $(this).closest('.modal').find('input[type=checkbox]').prop('checked', false);
                $(this).prop('checked', true);
                $(this).prop('disabled', false)
            }
            else {
                $(this).closest('.modal').find('input[type=checkbox]').prop('disabled', false);
            }
        })
        $('.cbx-quyen-cha').off('change').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).parent().find('.cbx-quyen-con').prop('checked', true);
            }
            else {
                $(this).parent().find('.cbx-quyen-con').prop('checked', false);
            }
        })
        $('#luu-quyen-user').off('click').on('click', function () {
            var userId = $(this).data('userid');
            var checkedQuyen = [];

            $('.cbx-quyen-con').each(function (e) {
                if ($(this).is(':checked')) {
                    var q = $(this).data('roleid');
                    checkedQuyen.push(q);
                }
            })
            $('.cbx-quyen-all').each(function (e) {
                if ($(this).is(':checked')) {
                    var q = $(this).data('roleid');
                    checkedQuyen.push(q);
                }
            })
            var parameters = {
                UserId: userId,
                RoleIds: checkedQuyen
            }
            R.Users.LuuQuyenByUserId(parameters);
        })
        $('#btn-tim-kiem-user').off('click').on('click', function () {
            R.Users.Filter();
        })

    },
    PhanTrang: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        R.Pagination($binding_phan_trang, index, total, size, function () {
            /*alert('An phan trang');*/
            //R.Users.Filter();
            //console.log('Da an nut phan trang');
            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            console.log(pageNum);
            R.Users.Filter(pageNum);
            R.Users.RegisterEvent();
            
        })
    },
    OpenPhanQuyenModal: function (userId) {
        var url = "/UserRoles/ModalPhanQuyen?userId=" + userId
        $.get(url, function (response) {
            $('#phan-quyen-user-modal').html('').html(response);
            $('#phan-quyen-user-modal').modal('show');
            R.Users.RegisterEvent();
        })
    },
    LuuQuyenByUserId: function (parameters) {
        console.log(parameters);
        var url = "/UserRoles/SaveQuyenByUserId"
        $.post(url, parameters, function (response) {
            alert('Cập nhật quyền thành công');
            $('#phan-quyen-user-modal').modal('hide');
        })
    },
    OpenGetByIdModal: function (userId) {
        var url = "/Users/ModalGetById?userId=" + userId
        $.get(url, function (response) {
            $('#getbyid-modal').html('').html(response);
            $('#getbyid-modal').modal('show');
            console.log(response);
            R.Users.RegisterEvent();
        })
    },
    Filter: function (index) {
        var keyword = $('#txb-keyword-user').val();
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = index ?? 1;
        //var size = $div_phan_trang_container.data('size');
        var size = 10;
        var params = {
            Keyword: keyword,
            Index: index,
            Size: size
        }
        var url = '/Users/GetListUser'
        $.post(url, params, function (response) {
            $('.binding-data').html('').html(response);
            R.Users.PhanTrang();
            //var $binding_phan_trang = $('.binding-phan-trang');
            //R.SetCurrentPage($binding_phan_trang, index);
            R.Users.RegisterEvent();
        })
    }
}
R.Users.Init();