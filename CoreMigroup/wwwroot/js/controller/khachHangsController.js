﻿R.KhachHang = {
    Init: function () {
        R.KhachHang.InitalLibrary();
        R.KhachHang.RegisterEvent();
        R.KhachHang.LoaiKhachHang = {};
        R.KhachHang.NguonKhachHang = {};
        R.KhachHang.LoadKhachHang();
    },
    RegisterEvent: function () {
        $('#btn-create-kh').off('click').on('click', function () {
            R.KhachHang.ViewAddOrEdit(0);
        })

        $('#save-khach-hang-form').off('submit').on('submit', function () {
            var id = $(this).data('id');
            R.KhachHang.AddOrEdit_KhachHang(id);
            return false;
        })

        $('#btn-tim-kiem-kh').off('click').on('click', function () {
            R.KhachHang.LoadKhachHang();
        })

        $('.edit-khach-hang').off('click').on('click', function () {
            var id = $(this).data('id');
            console.log(id);
            R.KhachHang.ViewAddOrEdit(id);
        })
    },
    InitalLibrary: function () {
        //slb-dm-kh-search
        //slb-nguon-kh-search
        $('#slb-dm-kh-search').select2();
        $('#slb-nguon-kh-search').select2();
        $('#txb-thoi-gian-nhap-dl').daterangepicker({
            locale: { format: 'DD/MM/YYYY' },
            autoUpdateInput: false

        })
        $('#txt-ngay-sinh').daterangepicker({
            locale: { format: 'DD/MM/YYYY' },
            autoUpdateInput: true,
            singleDatePicker: true,
            showDropdowns: true
        })

        var $loaiKH = $('#slb-chon-loai-khach-hang');
        

        var data_loai_kh = $loaiKH.data('danhmuc');
        
        if (typeof data_loai_kh !== 'undefined') {
            console.log('checker')
            /*console.log(data_loai_kh*//*)*/
            data_loai_kh = data_loai_kh.filter(x => x.value !== 'null');
            console.log(data_loai_kh);
        }
        R.InitTagInput($loaiKH,
            data_loai_kh,
            (value) => {
                if (typeof value == "number") {
                    console.log(1);
                    R.KhachHang.LoaiKhachHang.Text = "";
                }
                if (typeof value == "string") {
                    console.log(1);
                    R.KhachHang.LoaiKhachHang.Text = value;
                }
            },
            (value) => {
                if (R.KhachHang.LoaiKhachHang.Id !== "undefined") {
                    R.KhachHang.LoaiKhachHang = {};
                }
            }
        )
        $loaiKH.parent().find('.amsify-suggestags-list').css('width', '90%')
        var $nguonKH = $('#slb-nguon-khach-hang');
        var data_nguon_kh = $nguonKH.data('nguon');
        if (typeof data_nguon_kh !== 'undefined') {
            data_nguon_kh = data_nguon_kh.filter(x => x.value !== 'null');
            console.log(data_nguon_kh);
        }
        R.InitTagInput($nguonKH,
            data_nguon_kh,
            (value) => {
                if (typeof value == "number") {
                    console.log(1);
                    R.KhachHang.NguonKhachHang.Text = "";
                }
                if (typeof value == "string") {
                    console.log(1);
                    R.KhachHang.NguonKhachHang.Text = value;
                }
            },
            (value) => {
                if (R.KhachHang.NguonKhachHang.Id !== "undefined") {
                    R.KhachHang.NguonKhachHang = {};
                }
            }
        )
        $nguonKH.parent().find('.amsify-suggestags-list').css('width', '90%')
        $('#slb-chon-nhan-vien-phu-trach').select2()

    },
    ViewAddOrEdit: function (id) {
        var url = "/KhachHangs/ViewAddOrEdit"
        var params = {
            id: id
        }
        $.post(url, params, function (response) {
            $('#save-khach-hang-modal').html('').html(response);
            R.KhachHang.InitalLibrary();
            $('#save-khach-hang-modal').modal('show');
            R.KhachHang.RegisterEvent();
        })
    },
    AddOrEdit_KhachHang: function (id) {

        var Date = $('#txt-ngay-sinh').val();
        Date = Date.replace(' ', '').split('/');

        var Id = id;
        var Ten = $('#txt-ten').val();
        var GioiTinh = $('input[name="radio-gioi-tinh"]:checked').val();
        var NgaySinh = Date[2] + "-" + Date[1] + "-" + Date[0]
        var DiaChi = $('#txt-dia-chi').val();
        var Email = $('#txt-email').val();
        var CMND = $('#txt-cmnd').val();
        var SDT = $('#txt-so-dien-thoai').val();
        var LoaiKH = $('#slb-chon-loai-khach-hang').val();
        var NguonKH = $('#slb-nguon-khach-hang').val();
        var NhanVienPTId = $('select[name=slb-chon-nhan-vien-phu-trach] option').filter(':selected').val();
        
        
        var params = {
            id : Id,
            NhanVienPTId : NhanVienPTId,
            NgaySinh : NgaySinh,
            Ten : Ten,
            GioiTinh : GioiTinh,
            DiaChi : DiaChi,
            Email : Email,
            SoDT : SDT,
            CMND : CMND,
            LoaiKH : LoaiKH,
            NguonKH : NguonKH
        }
        console.log(params.idNhanVienPTId + " " + params.LoaiKH);
        var url = "/KhachHangs/AddOrEdit_KhachHang";

        $.post(url, params, function (response) {
            $('#save-khach-hang-modal').modal('hide');
            R.PopupNotify("Upload file thành công", "success");
            R.KhachHang.LoadKhachHang();
            R.KhachHang.RegisterEvent();
        })
    },
    LoadKhachHang: function (index) {
        var index = index ?? 1;
        var size = 10;
        var DanhMuc = [];
        var Nguon = [];
        var date = $('#txb-thoi-gian-nhap-dl').val();
        $('#slb-dm-kh-search').val().forEach(function (d) {
            DanhMuc.push(d);
        })
        $('#slb-nguon-kh-search').val().forEach(function (m) {
            Nguon.push(m);
        })

        var ngayTaoTu = null;
        var ngayTaoDen = null;
        var start = date.split('-')[0];
        if (start !== undefined) {
            start = start.replace(' ', '').split('/');
            ngayTaoTu = start[2] + "-" + start[1] + "-" + start[0];
        }

        var end = date.split('-')[1];
        if (end !== undefined) {
            end = end.replace(' ', '').split('/');
            ngayTaoDen = end[2] + "-" + end[1] + "-" + end[0];
        }

        var keyWord = $('#txb-keyword-khach-hang').val();
        var params = {
            Index: index,
            Size: size,
            DanhMuc: DanhMuc,
            Nguon: Nguon,
            NgayTaoTu: ngayTaoTu,
            NgatTaoDen: ngayTaoDen,
            KeyWord: keyWord
        }
        var url = "/KhachHangs/FilterKhachHang";
        $.post(url, params, function (response) {
            $('.binding-data').html('').html(response);
            R.KhachHang.PhanTrangFile();
            R.KhachHang.RegisterEvent();
        })
    },
    PhanTrangFile: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        R.Pagination($binding_phan_trang, index, total, size, function () {

            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            R.KhachHang.LoadKhachHang(pageNum);
            R.KhachHang.RegisterEvent();
        })

    }
}
R.KhachHang.Init();