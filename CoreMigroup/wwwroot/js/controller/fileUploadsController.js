﻿

R.FileUpload = {
    Init: function () {
        R.FileUpload.InitalLibrary();
        R.FileUpload.LoadFileUploadList();
        R.FileUpload.RegisterEvent();
        R.FileUpload.ChoosenFolder = {};
    },
    RegisterEvent: function () {

        $('#btn-tim-kiem-file').off('click').on('click', function () {

            R.FileUpload.LoadFileUploadList();

        })
        $('#create-file-upload').off('click').on('click', function () {
            R.FileUpload.ViewAddOrEdit(0);
        })
        $('.edit-file-upload').off('click').on('click', function () {
            var id = $(this).data('id');
            R.FileUpload.ViewAddOrEdit(id);
        })
        $('#save-file-upload-form').off('submit').on('submit', function () {
            var id = $(this).data('id');
            R.FileUpload.AddOrEdit(id);
            return false;
        })
        $('.dong-bo-du-lieu-file-upload').off('click').on('click', function () {
            var id = $(this).data('id');
            R.FileUpload.DongBoDuLieu(id);
        })
        $('.applyBtn').off('click').on('click', function () {

            $('#slb-folder-search').select2();
            $('#txb-thoi-gian-upload').daterangepicker({
                locale: { format: 'DD/MM/YYYY' },
                autoUpdateInput: true

            })
        })
    },

    DongBoDuLieu: function (id) {
        var url = "/FileUploads/DongBoDuLieu_DoanhNghiep";
        var params = {
            id: id
        }
        $.post(url, params, function (response) {
            R.PopupNotify("Đồng bộ dữ liệu thành công!", "success");
        })
    },
    AddOrEdit: function (id) {
        var id = id;
        var ten = $('#txt-ten-file-upload').val();
        var mota = R.GetValueCkeditor('txt-mo-ta-file-upload');
        var danhMuc = $('#slb-folder-file-upload').val();
        var danhMucTen = "";
        var files = $('#f-chon-file-upload').prop('files');
        var folder = R.FileUpload.ChoosenFolder;
        if (folder.Id > 0) {
            danhMuc = folder.Id
        }
        else {
            if (folder.Text !== "undefined" || folder.Text != "") {
                danhMuc = 0;
                danhMucTen = folder.Text;
            }
        }
        /*var */

        //Bat dau save
        var filesInput = Object.values(files);
        var dt = new FormData();
        filesInput.forEach(function (e) {
            dt.append('lsFiles', e);
        })
        dt.append('tenFile', ten);
        dt.append('motaFile', mota);
        dt.append('danhMucFile', danhMuc);
        dt.append('danhMucTen', danhMucTen);
        dt.append('id', id);
        var url = "/FileUploads/AddOrUpdateFileUpload_DangKyDoanhNghiep";
        $.ajax({
            method: 'POST',
            url: url,
            contentType: false,
            processData: false,
            data: dt,
            success: function (response) {
                $('#save-file-upload-modal').modal('hide');
                R.PopupNotify("Upload file thành công", "success");
                R.FileUpload.LoadFileUploadList();
                R.FileUpload.RegisterEvent();
                return false;

            },
            error: function (response) {
                R.FileUpload.RegisterEvent();
            }
        })

    },
    InitalLibrary: function () {
        $('#slb-folder-search').select2();
        $('#txb-thoi-gian-upload').daterangepicker({
            locale: { format: 'DD/MM/YYYY' },
            autoUpdateInput: false

        })
        //$('#tag-folder-file-upload').TokenAutocomplete({
        //    initialTokens: ['css', 'script', 'com'],
        //    noMatchesText: 'No matching results...'
        //})
    },
    LoadFileUploadList: function (index) {

        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = index ?? 1;
        var size = 10;
        var lsFolder = [];
        var date = $('#txb-thoi-gian-upload').val();
        $('#slb-folder-search').val().forEach(function (e) {
            lsFolder.push(parseInt(e));
        })
        var ngayTaoTu = null;
        var ngayTaoDen = null;
        var start = date.split('-')[0];
        if (start !== undefined) {
            start = start.replace(' ', '').split('/');
            ngayTaoTu = start[2] + "-" + start[1] + "-" + start[0];
        }

        var end = date.split('-')[1];
        if (end !== undefined) {
            end = end.replace(' ', '').split('/');
            ngayTaoDen = end[2] + "-" + end[1] + "-" + end[0];
        }


        var keyWord = $('#txb-keyword-file-upload').val();
        var params = {
            FolderIds: lsFolder,
            NgayUploadTu: ngayTaoTu,
            NgayUploadDen: ngayTaoDen,
            Keyword: keyWord,
            Index: index,
            Size: size
        }
        var url = "/FileUploads/FilterFiles";
        $.post(url, params, function (response) {
            //console.log(response);
            $('.binding-data').html('').html(response);
            R.FileUpload.PhanTrangFile();
            //var $binding_phan_trang = $('.binding-phan-trang');
            //R.SetCurrentPage($binding_phan_trang, index);
            R.FileUpload.RegisterEvent();
        })
    },
    ViewAddOrEdit: function (id) {
        id = id ?? 0;
        var url = "/FileUploads/ViewAddEditFileUpload";
        var params = {
            id: id
        }
        $.post(url, params, function (response) {
            $('#save-file-upload-modal').html('').html(response);
            R.InitCkeditor('txt-mo-ta-file-upload');
            //Init code TagInput
            var el = $('#tag-folder');
            //var data = JSON.parse();
            var data = el.data('danhmuc');
            //tag-folder
            R.InitTagInput(el
                , data
                , (value) => {
                    if (typeof value == "number") {
                        R.FileUpload.ChoosenFolder.Id = value;
                        R.FileUpload.ChoosenFolder.Text = "";
                    }
                    if (typeof value == "string") {
                        R.FileUpload.ChoosenFolder.Id = 0;
                        R.FileUpload.ChoosenFolder.Text = value;
                    }
                }
                , (value) => {
                    if (R.FileUpload.ChoosenFolder.Id !== "undefined") {
                        R.FileUpload.ChoosenFolder = {};
                    }
                })
            //End tag input
            $('#save-file-upload-modal').modal('show');
            R.FileUpload.RegisterEvent();
        })
    },
    PhanTrangFile: function () {
        var $div_phan_trang_container = $('.chi-tiet-phan-trang');
        var index = $div_phan_trang_container.data('index');
        var size = $div_phan_trang_container.data('size');
        var total = $div_phan_trang_container.data('total');
        var $binding_phan_trang = $('.binding-phan-trang');
        R.Pagination($binding_phan_trang, index, total, size, function () {

            var pageNum = $('.binding-phan-trang').find('.active').first().data('num');
            R.FileUpload.LoadFileUploadList(pageNum);
            R.FileUpload.RegisterEvent();
        })

    }

}
R.FileUpload.Init();