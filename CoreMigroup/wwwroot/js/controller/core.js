﻿var R = {
    Init: function () { },
    RegisterEvent: function () {

        $('input[type=text]').off('keydown').on('keydown', function (evt) {
            if ($(this).data('isnumber') == 1) {
                
                var d = $(this).val();
                var converted = R.FormatNumberInput(d);
                $(this).val(converted);
                $(this).data('giatri', parseInt(converted.replace('.','')));
            }
        })

    },
    //Phan Phan Trang
    Pagination: function (el, index, total, size, callback) {
        var arr = [];
        for (var i = 1; i <= total; i++) {
            arr.push(i);
        }

        el.pagination({
            dataSource: arr,
            pageNumber: index,
            pageSize: size,
            showPrevious: false,
            showNext: false,
            afterPageOnClick: callback,

        })
    },
    SetCurrentPage: function (elbindingPhanTrang, index) {
        
        elbindingPhanTrang.find('.J-paginationjs-page').each(function (e) {
            $(this).removeClass('active');
        });
        elbindingPhanTrang.find('.J-paginationjs-page').each(function (e) {
            
            if ($(this).data('num') == index) {
                $(this).addClass('active');
            }
                
        });
    },
    //Phan thong bao
    Notify: function (el, text, type) {
        if (el == null) {
            $.notify(text, type);
        }
        if (el != null) {
            $.notify(el, text, type);
        }
    },
    //Phan Ckeditor
    InitCkeditor: function (name_el) {
        CKEDITOR.replace(name_el);
    },
    InitTagInput: function (el, arrayData, afterAdd, afterRemove) {
        el.amsifySuggestags({
            //type: 'bootstrap',
            suggestions: arrayData,
            tagLimit: 1,
            afterAdd: function (value) {
                if (typeof afterAdd === "function")
                    afterAdd(value)
            },
            afterRemove: function (value) {
                if (typeof afterRemove === "function")
                    afterRemove(value)
            },
            defaultTagClass: 'badge'
        })
    },
    GetValueCkeditor: function (name_el) {
        return CKEDITOR.instances[name_el].getData();
    },
    PopupNotify: function (noi_dung, loai_notify) {
        $.notify(noi_dung, loai_notify);
    },
    Confirm: function () {
        var rs = confirm("Want to delete?");
        return rs;
    },
    FormatNumberInput: function (nStr) {
        var t = nStr.replace(/\./g, '');
        R.RegisterEvent();
        return t.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        //return t.toString().replace(/(?=(\d\d\d)+(?!\d))/g, "$1.");
    }


}
R.Init();

/*
 var total_create = $('#txt_total_amount_create').val().replace(/\./g, '');
    var real_create = $('#txt_receiver_total_amount_create').val().replace(/\./g, '');
    if (real_create > 0 && total_create > 0) {
        var miss = parseInt(total_create) - parseInt(real_create);

        $('#txt_miss_create').val(miss.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    }
    else {
        $('#txt_miss_create').val('0.0');
    }
 */